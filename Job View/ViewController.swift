//
//  ViewController.swift
//  Job View
//
//  Created by Joshua Gill on 14/02/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import Alamofire
import FirebaseDatabase
import Fabric
import TwitterKit
import SwiftSpinner


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, EasyTipViewDelegate {
    
    //Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var loggedInUser_lbl: UILabel!
    
    @IBOutlet weak var questionnaireButton: UIButton!
    //Variables
    let userDefaults = UserDefaults.standard
    var jobRole = "IT"
    var row = 0
    let generator = UIImpactFeedbackGenerator(style: .light)
    var logoURLArray = [String]()
    var RecommendJobsArray = [jobs]()
    typealias JSONStandard = [String : AnyObject]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RecommendJobsArray.removeAll()
        
        
        SwiftSpinner.show("Loading data...")
        
        //Set the table view background to transparent
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.separatorColor = UIColor.clear
        
        //Set Up Tips
        setUpTip()

        //Open the Menu
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        //Check to see if a user is logged in
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil {
                //See if the user is logged in via twitter
                if dataHandler.getString(saveName: "twitter") != ""{
                    //Set the display name to the twitter name
                    self.loggedInUser_lbl.text = dataHandler.getString(saveName: "twitter")
                }
                else{
                    //Set the dispplay name to the user's email address
                    self.loggedInUser_lbl.text = FIRAuth.auth()?.currentUser?.email
                }
                
                //Set the local job role
                self.jobRole = dataHandler.getString(saveName: "localJobRole")
                
                //Load the data and sort the table
                self.fetchData()
                
                self.tableView.reloadData()
            } else {
                //If the user is not logged in, load the data using the defaults
                self.loggedInUser_lbl.text = "Not logged in"
                self.jobRole = dataHandler.getString(saveName: "localJobRole")
                self.fetchData()
                
                self.tableView.reloadData()
            }
            
            if self.jobRole == "IT"{
                self.setUpTip()
                EasyTipView.show(forView: self.questionnaireButton, withinSuperview: self.questionnaireButton.superview, text: "Discover your ideal job role through the Job View Questionnaire! \n -Tap to Dismiss-")
            }
        }
        self.tableView.reloadData()
        
        //set up the table
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        

    }
    
    //Clear the table delegate when the class is dropped
    deinit {
        self.tableView.emptyDataSetSource = nil
        self.tableView.emptyDataSetDelegate = nil
    }
    
    //Show Tip
    func setUpTip(){
        
        var preferences = EasyTipView.Preferences()
        preferences.drawing.foregroundColor = UIColor.black
        preferences.drawing.backgroundColor = UIColor.white
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top
        preferences.animating.showInitialAlpha = 0
        
        EasyTipView.globalPreferences = preferences
        
    }
    
    //Dismiss Tip
    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        print("\(tipView) did dismiss!")
    }
   
    
    //Reques the job data from Reed
    func requestJobData(){
        
        let URL = "https://www.reed.co.uk/api/1.0/search?"
        var location = dataHandler.getString(saveName: "localLocation")
        
        //Check the location is not Nil
        if location == ""{
            location = "London"
        }
        
        //Build the parameters for the request
        let parameters: Parameters = [
            "keywords": jobRole,
            "locationName": location,
            "distanceFromLocation": "100",
            "graduate": "true"
        ]
        
        //Set the credentials for the request
        let user = "927d498d-8736-499a-bf97-7d3244996129"
        let password = ""
        
        //Make request, then send the data onto the parser
        Alamofire.request(URL, parameters :parameters).authenticate(user: user, password: password).responseJSON(completionHandler: {
            response in
            self.parseData(JSONData: response.data!)
            
        })
        
    }
    
    func parseData(JSONData : Data){
        RecommendJobsArray.removeAll()
        do {
            guard let teamJSON =  try JSONSerialization.jsonObject(with: JSONData, options: []) as? [String: Any],
                let jobJSONData = teamJSON["results"] as? [[String: Any]]
                else { return }
            
            //looping through all the json objects in the array teams
            for i in 0 ..< jobJSONData.count{
                
                //Capturing the data for that row
                let jobID = jobJSONData[i]["jobId"] as! Int
                let jobTitle = (jobJSONData[i]["jobTitle"] as! NSString) as String
                let applications = jobJSONData[i]["applications"] as! Int
                let employerName = jobJSONData[i]["employerName"] as! String
                let locationName = jobJSONData[i]["locationName"] as! String
                let jobDescription = jobJSONData[i]["jobDescription"] as! String
                let jobUrl = jobJSONData[i]["jobUrl"] as! String
                
                //Get Dates as strings
                let expirationDateString = jobJSONData[i]["expirationDate"] as! String
                let addedDateString = jobJSONData[i]["date"] as! String
                
                //Convert dates to NSDate format
                let inputFormatter = DateFormatter()
                inputFormatter.dateFormat = "dd/MM/yyyy"
                let expirationDate = inputFormatter.date(from: expirationDateString)! as NSDate
                let addedDate = inputFormatter.date(from: addedDateString)! as NSDate
                
                //Check to see if the salaries is not nil
                var maxSalary = 0
                if jobJSONData[i]["maximumSalary"] as? Int != nil{
                    maxSalary = jobJSONData[i]["maximumSalary"] as! Int
                }
                
                var minSalary = 0
                if jobJSONData[i]["minimumSalary"] as? Int != nil{
                    minSalary = jobJSONData[i]["maximumSalary"] as! Int
                }
                
                //Get linkedIn data
                    //Build request
                var logoURL = ""
                let URL = "https://www.linkedin.com/ta/federator?"
                let parameters: Parameters = [
                    "query": employerName,
                    "types": "company"
                ]
                //Make request
                Alamofire.request(URL, parameters :parameters).responseJSON(completionHandler: {
                    response in
                    do{
                        let readableLogoJSON = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! JSONStandard
                        //Extract Data
                        if let companies = readableLogoJSON["company"] as? JSONStandard{
                            if var results = companies["resultList"] as? [JSONStandard]{
                                //print(results)
                                if results.isEmpty == false {
                                    let result = results[0]
                                    //Get data from first Result
                                    if result["imageUrl"] as? String != nil{
                                        logoURL = result["imageUrl"] as! String
                                        let linkedIn = result["url"] as! String
                                        
                                        //Save data to jobs array
                                        self.RecommendJobsArray.append(jobs.init(jobID: String(jobID), jobTitle: jobTitle, employerName: employerName, jobLocation: locationName, jobDescription: jobDescription, applications: applications , jobUrl: jobUrl, minSalary: minSalary, maxSalary: maxSalary, logoURL: logoURL, linkedIn: linkedIn, dateEnd: expirationDate, dateAdded: addedDate))
                                        self.saveData()
                                        self.tableView.reloadData()
                                    }
                                }
                            }
                        }
                    }
                    catch{
                        //Save jobs data if linkedIn API is unavaliable
                        self.RecommendJobsArray.append(jobs.init(jobID: String(jobID), jobTitle: jobTitle, employerName: employerName, jobLocation: locationName, jobDescription: jobDescription, applications: applications , jobUrl: jobUrl, minSalary: minSalary, maxSalary: maxSalary, logoURL: logoURL, linkedIn: "", dateEnd: expirationDate, dateAdded: addedDate))
                        //Alert the user as to why the data is missing
                        self.alert(title: "LINKEDIN API", message: "API Error")
                        print(error)
                        self.tableView.reloadData()
                    }
                })
            }
        } catch {
            //Alert the user that there is an issue with the API
            alert(title: "REED API", message: "API Error")
            print(error)
            self.tableView.reloadData()
        }
        saveData()
        self.tableView.reloadData()
        SwiftSpinner.hide()
    }
    
    //Open the side menu from the menu button
    @IBAction func openMenu(_ sender: Any) {
        self.generator.impactOccurred()
        self.revealViewController().revealToggle(sender)
    }
    
    func checkForDuplicates(){
        print("Checking...")
        var array = [Int]()
        for i in 0 ..< RecommendJobsArray.count  {
            for p in 0 ..< RecommendJobsArray.count  {
                if RecommendJobsArray[i].jobID == RecommendJobsArray[p].jobID{
                    print("Found")
                    array.append(i)
                }
            }
        }
        
        for i in 0 ..< array.count  {
            RecommendJobsArray.remove(at: array[i])
        }
        
        saveData()
        self.tableView.reloadData()
        SwiftSpinner.hide()
        
    }
    
    //Check to see if the saved data is less than an hour old, if so, use the old data
    func fetchData(){
        
        let currentDate = dataHandler.getDateTime()
        var oldDate = dataHandler.getInt(saveName: "date")
        
        oldDate = 0
        
        if oldDate == 0{
            print("New Data from nil date")
            requestJobData()
        }else{
        
            //Check to see if the data is old
            if oldDate < currentDate{
                print("New Data")
                requestJobData()
            }else{
                print("Old Data")
                RecommendJobsArray=dataHandler.getJobsArray(saveName: "encodedJobsArrayData")
                self.tableView.reloadData()
                //Check if the job role has changed
                if dataHandler.getString(saveName: "searchedJobRole") != dataHandler.getString(saveName: "localJobRole"){
                    print("Job Role Changed, New Data")
                    requestJobData()
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    //Save data locally
    func saveData(){
        let currentDate = dataHandler.getDateTime()
        dataHandler.saveJobsArray(JobsArray: RecommendJobsArray, saveName: "encodedJobsArrayData")
        dataHandler.saveObject(item: currentDate, saveName: "date")
        dataHandler.saveObject(item: jobRole, saveName: "searchedJobRole")
    }
    
    //Function to calculate how many days are remaining on a job role
    func daysBetweenDates(startDate: Date, endDate: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([Calendar.Component.day], from: startDate, to: endDate)
        return components.day!
    }
    

    //Get table size
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var nor = 0
        
        //Only return 3 results
        if RecommendJobsArray.count > 3{
            nor = 3
        }
        else{
            nor = RecommendJobsArray.count
        }
        return nor
    }
    
    //Load data onto the table
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = Bundle.main.loadNibNamed("JobTableViewCell", owner: self, options: nil)?.first as! JobTableViewCell

        //Download Logo       
        cell.logo_image.sd_setImage(with: URL(string: RecommendJobsArray[indexPath.row].logoURL), placeholderImage: nil, options: [.continueInBackground, .progressiveDownload])
        cell.logo_image.layer.cornerRadius = 8.0
        cell.logo_image.clipsToBounds = true
        
        //Load the location, title and applicaitons data
        cell.location_lbl.text = RecommendJobsArray[indexPath.row].jobLocation
        cell.title_lbl.text=RecommendJobsArray[indexPath.row].jobTitle
        cell.applications_lbl.text=String(RecommendJobsArray[indexPath.row].applications)
        
        //Check if for fav and viewed
        if dataHandler.checkFavs(jobID: RecommendJobsArray[indexPath.row].jobID) == true {
            print(dataHandler.checkFavs(jobID: RecommendJobsArray[indexPath.row].jobID))
            cell.sideColour_lbl.backgroundColor = UIColor.orange
        }else if dataHandler.checkViewed(jobID: RecommendJobsArray[indexPath.row].jobID) == true{
            cell.sideColour_lbl.backgroundColor = UIColor.blue
        }
        
        //Format the applcations number
        if RecommendJobsArray[indexPath.row].applications < 20{
            cell.applications_lbl.textColor = UIColor.green
        }else if RecommendJobsArray[indexPath.row].applications < 50{
            cell.applications_lbl.textColor = UIColor.orange
        }else{
            cell.applications_lbl.textColor = UIColor.red
        }
        
        //Format the salary to the local currancy
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        let minSalary = RecommendJobsArray[indexPath.row].minSalary as NSNumber
        let maxSalary = RecommendJobsArray[indexPath.row].maxSalary as NSNumber
        cell.salary_lbl.text = formatter.string(from: minSalary)! + " to " + formatter.string(from: maxSalary)!
        
        
        //Get Remaining Days
        let date1 = RecommendJobsArray[indexPath.row].dateEnd as Date
        let date2 = Date()
        cell.daysLeft_lbl.text = "Days Left: " + String(daysBetweenDates(startDate: date2, endDate: date1))
        
        //Set the cell background colour
        cell.background_lbl.layer.masksToBounds = true
        //cell.background_lbl.layer.cornerRadius = 10
        cell.sideColour_lbl.layer.masksToBounds = true
        //cell.sideColour_lbl.roundCorners(corners: [.topLeft, .bottomLeft], radius: 10)
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    
    
    //Set row height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        //Generate haptic feedback when a user selects a row
        let generator = UISelectionFeedbackGenerator()
        generator.selectionChanged()
        
        row = indexPath.row
        
        //Load the job details row for the selected row
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "segueThree", sender: self)
  
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //Transfer the job details to the job details view
        if segue.identifier == "segueThree"{
            self.generator.impactOccurred()
            let myVC = segue.destination as? JobDetailsViewController
            myVC?.job = RecommendJobsArray[row]
            myVC?.searchString = jobRole
            myVC?.currentPickerRow = 0
            navigationController?.pushViewController(myVC!, animated: true)
        }
    }
    
    //hide empty cells
    override func viewWillAppear(_ animated: Bool) {
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    //Add a swipe gesture to the cell to allow the user to favourite a role
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        //Add favorite to the cell
        let favorite = UITableViewRowAction(style: .normal, title: "Favorite") { action, index in
            
            //Check to see if the user is logged in
            FIRAuth.auth()?.addStateDidChangeListener { auth, user in
                if user != nil {
                    if dataHandler.checkFavs(jobID: String(self.RecommendJobsArray[indexPath.row].jobID)) == false{
                        //Add the job
                        dataHandler.favAJob(jobID: String(self.RecommendJobsArray[indexPath.row].jobID))
                        let indexPath = IndexPath(item: indexPath.row, section: 0)
                        tableView.reloadRows(at: [indexPath], with: .fade)
                        let currentCell = tableView.cellForRow(at: indexPath) as! JobTableViewCell
                        currentCell.sideColour_lbl.backgroundColor = UIColor.orange
                        self.generator.impactOccurred()
                        
                    }else{
                        //Remove the job
                        dataHandler.removeAJob(jobID: String(self.RecommendJobsArray[indexPath.row].jobID))
                        let indexPath = IndexPath(item: indexPath.row, section: 0)
                        tableView.reloadRows(at: [indexPath], with: .fade)
                        let currentCell = tableView.cellForRow(at: indexPath) as! JobTableViewCell
                        currentCell.sideColour_lbl.backgroundColor = UIColor.clear
                        
                    }
                    
                } else {
                    //Alert the user to login to add to their favourites
                    self.alert(title: "Favourites", message: "Please login to save your favourites.")
                }
            }
        }
        
        //Set the favorite icon's background colour
        favorite.backgroundColor = UIColor.orange
        
        return [ favorite ]
    }

    //Alert Function
    func alert(title:String,message:String){
        generator.impactOccurred()
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }


}




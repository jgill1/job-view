//
//  Roles.swift
//  Job View
//
//  Created by Joshua Gill on 07/03/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import Foundation
class roles {
    
    var roleTitle: String
    var roleDescription: String
    var otherRoles: [String]
    
    
    init(roleTitle: String, roleDescription: String, otherRoles: [String]) {
        self.roleTitle = roleTitle
        self.roleDescription = roleDescription
        self.otherRoles = otherRoles
    }
    
    class func getRoles()->[roles]{
        
        var rolesArray = [roles]()
        
        rolesArray.append(roles.init(roleTitle: "Software Tester", roleDescription: "Bugs can have a massive impact on the productivity and reputation of an IT firm. Testers try to anticipate all the ways an application or system might be used and how it could fail. \n \n They don’t necessarily program but they do need a good understanding of code. \n \n Testers prepare test scripts and macros, and analyse results, which are fed back to the project leader so that fixes can be made. \n \n Testers can also be involved at the early stages of projects in order to anticipate pitfalls before work begins. You can potentially get to a high level as a tester.", otherRoles: []))
        
        rolesArray.append(roles.init(roleTitle: "Software Developer", roleDescription: "The work of a software developer typically includes designing and programming system-level software: operating systems, database systems, embedded systems and so on. They understand how both software and hardware function. The work can involve talking to clients and colleagues to assess and define what solution or system is needed, which means there’s a lot of interaction as well as full-on technical work. \n \n Software Developers are often found in electronics and telecommunications companies.", otherRoles: ["Software Developer", "Web Developer", "Network Engineer"]))
        
        rolesArray.append(roles.init(roleTitle: "Technical Support", roleDescription: "These are the professional troubleshooters of the IT world. \n \n Many technical support specialists work for hardware manufacturers and suppliers solving the problems of business customers or consumers, but many work for end-user companies supporting, monitoring and maintaining workplace technology and responding to users’ requests for help. \n \n Some lines of support require professionals with specific experience and knowledge, but tech support can also be a good way into the industry for graduates.", otherRoles: []))
            
        rolesArray.append(roles.init(roleTitle: "Systems Analyst", roleDescription: "Systems analysts investigate and analyse business problems and then design information systems that provide a feasible solution, typically in response to requests from their business or a customer. \n \n They gather requirements and identify the costs and the time needed to implement the project. \n \n The job needs a mix of business and technical knowledge, and a good understanding of people. \n \n It’s a role for analyst programmers to move into and typically requires a few years’ experience from graduation.", otherRoles: ["Systems Analyst", "Project Manager"]))
        
        rolesArray.append(roles.init(roleTitle: "Business Analyst", roleDescription: "Business analysts are true midfielders, equally happy talking with technology people, business managers and end users. \n \n They identify opportunities for improvement to processes and business operations using information technology. \n \n The role is project based and begins with analysing a customer’s needs, gathering and documenting requirements and creating a project plan to design the resulting technology solution.", otherRoles: []))
        
        rolesArray.append(roles.init(roleTitle: "Network Engineer", roleDescription: "Network engineering is one of the more technically demanding IT jobs. \n \n Broadly speaking the role involves setting up, administering, maintaining and upgrading communication systems, local area networks and wide area networks for an organisation. \n \n Network engineers are also responsible for security, data storage and disaster recovery strategies. It is a highly technical role and you’ll gather a hoard of specialist technical certifications as you progress.", otherRoles: ["Network Engineer", "Software Developer", "Web Developer"]))
        
        rolesArray.append(roles.init(roleTitle: "Technical Sales", roleDescription: "Technical sales may be one of the least hands-on technical roles, but it still requires an understanding of how IT is used in business. \n \n You may sell hardware, or extol the business benefits of whole systems or services. \n \n Day to day, the job could involve phone calls, meetings, conferences and drafting proposals. \n \n There will be targets to meet and commission when you reach them.", otherRoles: []))
        
        rolesArray.append(roles.init(roleTitle: "Project Manager", roleDescription: "Project managers organise people, time and resources to make sure information technology projects meet stated requirements and are completed on time and on budget. \n \n They may manage a whole project from start to finish or manage part of a larger ‘programme’. \n \n It isn’t an entry-level role: project managers have to be pretty clued up. \n \n This requires experience and a good foundation of technology and soft skills, which are essential for working with tech development teams and high-level business managers.", otherRoles: ["Project Manager", "Systems Analyst"]))
        
        rolesArray.append(roles.init(roleTitle: "Web Developer", roleDescription: "Web development is a broad term and covers everything to do with building websites and all the infrastructure that sits behind them. \n \n The job is still viewed as the trendy side of IT years after it first emerged. \n \n These days web development is pretty technical and involves some hardcore programming as well as the more creative side of designing the user interfaces of new websites. \n \n The role can be found in organisations large and small.", otherRoles: ["Web Developer", "Network Engineer", "Software Developer"]))
        
        rolesArray.append(roles.init(roleTitle: "IT", roleDescription: "Take the questionnaire to discover your role!", otherRoles: []))

            
        return rolesArray
    }
    
    

}

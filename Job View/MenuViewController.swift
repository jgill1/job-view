//
//  MenuViewController.swift
//  Job View
//
//  Created by Joshua Gill on 14/02/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

class MenuViewController: UIViewController{
    
    //Outlets
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signOutButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Check to see if the user is logged in, if so, the sign out button will be displayed
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil {
                self.loginButton.isHidden = true
                self.signOutButton.isHidden = false
            } else {
                self.loginButton.isHidden = false
                self.signOutButton.isHidden = true
            }
        }
 
    }
    
    //Sign out action for the sign out button
    @IBAction func signOutButtonPressed(_ sender: Any) {
        
        //Sign out
        try! FIRAuth.auth()!.signOut()
        
        //Alert the user
        self.alert(title: "Authentication", message: "You have been logged out.")
        
        //Reset local data
        dataHandler.resetLocalDetails()

    }
    
    //Take the user to the login page
    @IBAction func loginButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "segueLogin", sender: self)
    }
    
    //Alert function
    func alert(title:String,message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

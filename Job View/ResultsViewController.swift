//
//  ResultsViewController.swift
//  Job View
//
//  Created by Joshua Gill on 17/02/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseCore
import UIKit
import CoreLocation

class ResultsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, CLLocationManagerDelegate, EasyTipViewDelegate {
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var findJobsButton: UIButton!
    @IBOutlet weak var locationField: UITextField!
    @IBOutlet weak var roleTitle: UILabel!
    @IBOutlet weak var roleDescription: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var picker_lbl: UILabel!
    @IBOutlet weak var pickerLine: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    var locationManager:CLLocationManager!
    
    var projectManager: Int?
    var productionSupport: Int?
    var businessAnalyst: Int?
    var softwareTester: Int?
    var softwareDeveloper: Int?
    var technicalSales: Int?
    var largestValue: Int?
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var currentCity = "London"
    var transfer = false
    
    var roleDetailsArray = roles.init(roleTitle: "", roleDescription: "", otherRoles: [])

    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView.isHidden = true
        picker_lbl.isHidden = true
        pickerLine.isHidden = true
        
        //setuptips
        setUpTip()
        
        self.locationField.text = dataHandler.getString(saveName: "localLocation")
        self.backgroundImageView.addBlurEffect()
        
        
        if transfer == false{
            
            if dataHandler.getString(saveName: "localJobRole") == "IT"{
                //Ask user to login or complete questionaire
                EasyTipView.show(forView: self.menuButton, withinSuperview: self.menuButton.superview, text: "Login or complete the questionnaire to find your role.")
            }else{
                //show user's current role
                findRoleDetails(RoleName: dataHandler.getString(saveName: "localJobRole"))
            }
            
        }else{
            //get user's role
            largestValue = getLargestValue()
            findResult()
        }
        
        //Open the Menu
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        roleDescription.textContainerInset = UIEdgeInsetsMake(10,10,10,10)
        roleDescription.layer.cornerRadius = 8
        
    }
    
    @IBAction func openMenu(_ sender: Any) {
        self.revealViewController().revealToggle(sender)
    }
    
    func getLargestValue()->Int{
        
        var largestValue = 0
        
        var sortArray: [Int] = [projectManager!, productionSupport!, businessAnalyst!, softwareTester!, softwareDeveloper!, technicalSales!]

        sortArray=sortArray.sorted(by: {$0 > $1})
        largestValue = sortArray[0]
        
        return largestValue

    }
    
    func findResult(){
        
        if projectManager == largestValue {
            findRoleDetails(RoleName: "Project Manager")
            print("projectManager")
        }else if productionSupport == largestValue {
            findRoleDetails(RoleName: "Technical Support")
            print("productionSupport")
        }else if businessAnalyst == largestValue {
            findRoleDetails(RoleName: "Business Analyst")
            print("businessAnalyst")
        }else if softwareTester == largestValue {
            findRoleDetails(RoleName: "Software Tester")
            print("softwareTester")
        }else if softwareDeveloper == largestValue {
            findRoleDetails(RoleName: "Software Developer")
            print("softwareDeveloper")
        }else if technicalSales == largestValue {
            findRoleDetails(RoleName: "Technical Sales")
            print("technicalSales")
        }else{
            print("Nothing Found")
        }
    }
    
    func findRoleDetails(RoleName: String){
        var rolesArray = roles.getRoles()
        
        for i in 0 ..< rolesArray.count  {
            if rolesArray[i].roleTitle == RoleName{
                roleDetailsArray.roleTitle = rolesArray[i].roleTitle
                roleDetailsArray.roleDescription = rolesArray[i].roleDescription
                roleDetailsArray.otherRoles = rolesArray[i].otherRoles
                loadDetails()
            }
        }
        
    }
    
    func loadDetails(){
        setRole(role: roleDetailsArray.roleTitle)
        roleTitle.text = roleDetailsArray.roleTitle
        roleDescription.text = roleDetailsArray.roleDescription
        
        if roleDetailsArray.otherRoles.count > 0{
            pickerView.isHidden = false
            picker_lbl.isHidden = false
            pickerLine.isHidden = false
            pickerView.reloadAllComponents()
        }
    }
    
    func alert(title:String,message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setRole(role:String){
        
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil {
                
                let ID = FIRAuth.auth()?.currentUser?.uid ?? "failed to get ID"
                
                let dataArray : [String : AnyObject] = [
                    "role" : role as AnyObject]
                
                let dbRef = FIRDatabase.database().reference()
                dbRef.child("users").child(ID).updateChildValues(dataArray)
                print("Sending Data")
            }else{
                 EasyTipView.show(forView: self.menuButton, withinSuperview: self.menuButton.superview, text: "Log in to access your profile from any device.")
            }
        }
        dataHandler.saveObject(item: role, saveName: "localJobRole")
    }
    
    func setLocation(location:String){
        
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil {
                
                let ID = FIRAuth.auth()?.currentUser?.uid ?? "failed to get ID"
                
                let dataArray : [String : AnyObject] = [
                    "location" : location as AnyObject]
                
                let dbRef = FIRDatabase.database().reference()
                dbRef.child("users").child(ID).updateChildValues(dataArray)
                print("Sending Data")
            }else{
                self.alert(title: "Account", message: "Log in to access your profile from any device.")
            }
        }
        dataHandler.saveObject(item: location, saveName: "localLocation")
        alert(title: "Location Update", message: "Your preferred location has been updated.")
    }
    
    // DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return roleDetailsArray.otherRoles.count
    }
    
    // Delegate
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = roleDetailsArray.otherRoles[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSForegroundColorAttributeName : UIColor.white])
        return myTitle
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        changeRole(newRoleRow: row)
        pickerView.selectRow(0, inComponent: 0, animated: true)
    }
    
    
    @IBAction func saveLocation(_ sender: Any) {
        setLocation(location: locationField.text!)
        locationField.endEditing(true)
        
    }
    @IBAction func getLocation(_ sender: Any) {
        determineMyCurrentLocation()
        
    }
    
    func changeRole(newRoleRow: Int){
        let newRole = roleDetailsArray.otherRoles[newRoleRow]
        var rolesArray = roles.getRoles()
        
        for i in 0 ..< rolesArray.count  {
            if rolesArray[i].roleTitle == newRole{
                roleDetailsArray.roleTitle = rolesArray[i].roleTitle
                roleDetailsArray.roleDescription = rolesArray[i].roleDescription
                roleDetailsArray.otherRoles = rolesArray[i].otherRoles
                loadDetails()
            }
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let point = CGPoint(x: 0,y :200)
        scrollView.setContentOffset(point,animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let point = CGPoint(x: 0,y :0)
        scrollView.setContentOffset(point,animated: true)
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        manager.stopUpdatingLocation()
        latitude = userLocation.coordinate.latitude
        longitude = userLocation.coordinate.longitude
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        getCity()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    
    func getCity(){
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: latitude, longitude: longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            // City
            if let city = placeMark.addressDictionary!["City"] as? NSString {
                print(city)
                print(city)
                self.currentCity = city as String
                self.setLocation(location: self.currentCity)
                self.locationField.text = self.currentCity
            }
            
        
        })
    }
    
    //Show Tip
    func setUpTip(){
        
        var preferences = EasyTipView.Preferences()
        preferences.drawing.foregroundColor = UIColor.black
        preferences.drawing.backgroundColor = UIColor.white
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.right
        preferences.animating.showInitialAlpha = 0
        
        EasyTipView.globalPreferences = preferences
        
    }
    
    //Dismiss Tip
    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        print("\(tipView) did dismiss!")
    }
}

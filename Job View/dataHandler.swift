//
//  dataHandler.swift
//  Job View
//
//  Created by Joshua Gill on 27/02/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth
import FirebaseDatabase

class dataHandler {
    
    class func getDateTime()->Int{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddhh"
        let currentDate = Int(formatter.string(from: date))
        //print(currentDate!)
        return currentDate!
    }
    
    class func saveJobsArray(JobsArray:[jobs], saveName: String){
        let userDefaults = UserDefaults.standard
        let encodedJobsArrayData: Data = NSKeyedArchiver.archivedData(withRootObject: JobsArray)
        userDefaults.set(encodedJobsArrayData, forKey: saveName)
        userDefaults.synchronize()
    
    }
    
    class func getJobsArray(saveName: String)->[jobs]{
        let userDefaults = UserDefaults.standard
        let decoded  = userDefaults.object(forKey: saveName) as! Data
        let jobsArray = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [jobs]
        
        return jobsArray
        
    }
    
    class func saveObject(item:Any, saveName: String){
        let userDefaults = UserDefaults.standard
        userDefaults.set(item, forKey: saveName)
    }
    
    
    class func getInt(saveName: String)->Int{
        let userDefaults = UserDefaults.standard
        var intData = 0
        
        if let myInt = userDefaults.value(forKey: saveName) as? Int {
            intData = myInt
        }
        
        return intData
    }
    
    class func getString(saveName: String)->String{
        let userDefaults = UserDefaults.standard
        var stringData = ""

        if let myString = userDefaults.value(forKey: saveName) as? String {
            stringData = myString
        }
        
        return stringData
    }
    
    class func getStringArray(saveName: String)->[String]{
        let userDefaults = UserDefaults.standard
        
        var stringArray = [String]()
        if((userDefaults.array(forKey: saveName)) != nil){
            stringArray = (UserDefaults.standard.array(forKey: saveName) as? [String])!
            
        }
        
        return stringArray
    }
    
    class func resetLocalDetails(){
        //Reset local data
        dataHandler.saveObject(item: "IT", saveName: "localJobRole")
        dataHandler.saveObject(item: "London", saveName: "localLocation")
        dataHandler.saveObject(item: "", saveName: "twitter")
        dataHandler.saveObject(item: "LoggedIn", saveName: "False")
        dataHandler.saveObject(item: [], saveName: "favJobsArray")
        dataHandler.saveObject(item: [], saveName: "viewedJobsArray")
        
    }
    
    class func getRole() {
        
        var userRole = "IT"
        
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil {
                FIRDatabase.database().reference().root.child("users").child("\(FIRAuth.auth()!.currentUser!.uid)").observeSingleEvent(of: .value, with: { snapshot in
                    if let snapDict = snapshot.value as? [String:AnyObject]{
                        userRole = snapDict["role"] as! String
                        dataHandler.saveObject(item: snapDict["role"] as! String, saveName: "localJobRole")
                        print("User Role from firebase...")
                        //print(userRole)
                    } else {
                        dataHandler.saveObject(item: "IT", saveName: "localJobRole")
                        print("No such user exists!.")
                    }
                })
            }
        }
        
        
    }
    
    class func getLocation() {
        
        var userLocation = "London"
        
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil {
                FIRDatabase.database().reference().root.child("users").child("\(FIRAuth.auth()!.currentUser!.uid)").observeSingleEvent(of: .value, with: { snapshot in
                    if let snapDict = snapshot.value as? [String:AnyObject]{
                        userLocation = snapDict["location"] as! String
                        dataHandler.saveObject(item: snapDict["location"] as! String, saveName: "localLocation")
                        print("User location from firebase...")
                        //print(userLocation)
                    } else {
                        dataHandler.saveObject(item: "London", saveName: "localLocation")
                        print("No such user exists!.")
                    }
                })
            }
        }
    }
    
    class func favAJob(jobID: String){
        
        
        let ID = FIRAuth.auth()?.currentUser?.uid ?? "fail"
        
        
        var favString = ""
        var favArray = [String]()
        
        FIRDatabase.database().reference().root.child("users").child("\(FIRAuth.auth()!.currentUser!.uid)").child("favJobs").observeSingleEvent(of: .value, with: { snapshot in
            if let snapDict = snapshot.value as? [String:AnyObject]{
                favString = snapDict["favJobs"] as! String
                
                //Convert String to String Array
                let string1 = favString.replacingOccurrences(of: "[", with: "", options: .literal, range: nil)
                let string2 = string1.replacingOccurrences(of: "]", with: "", options: .literal, range: nil)
                
                if string2.contains(", ") == true {
                    favArray = string2.components(separatedBy: ", ")
                    favArray.append(jobID)
                    
                }else{
                    
                    if (dataHandler.ContainsNumbers(s: string2).count > 0)
                    {
                        favArray = string2.components(separatedBy: ", ")
                        favArray.append(jobID)
                    }else{
                        favArray.removeAll()
                        favArray.append(jobID)
                    }
                }
                
                let intArray = favArray.map { Int($0)!}
                let dataArray : [String : AnyObject] = [
                    "favJobs" : String(describing: intArray) as AnyObject]
                
                let dbRef = FIRDatabase.database().reference()
                dbRef.child("users").child(ID).child("favJobs").setValue(dataArray)
                print("Sending Data")
                dataHandler.saveObject(item: favArray, saveName: "favJobsArray")
                
                
            } else {
                favArray.append(jobID)
                let intArray = favArray.map { Int($0)!}
                let dataArray : [String : AnyObject] = [
                    "favJobs" : String(describing: intArray) as AnyObject]
                
                let dbRef = FIRDatabase.database().reference()
                dbRef.child("users").child(ID).child("favJobs").setValue(dataArray)
                print("Sending Data")
                dataHandler.saveObject(item: favArray, saveName: "favJobsArray")
                
            }
        })
        
        
    }
    
    class func getFavJobs()->[String]{
        
        var favString = ""
        var favArray = [String]()
        
        FIRDatabase.database().reference().root.child("users").child("\(FIRAuth.auth()!.currentUser!.uid)").child("favJobs").observeSingleEvent(of: .value, with: { snapshot in
            if let snapDict = snapshot.value as? [String:AnyObject]{
                favString = snapDict["favJobs"] as! String
                
                //Convert String to String Array
                let string1 = favString.replacingOccurrences(of: "[", with: "", options: .literal, range: nil)
                let string2 = string1.replacingOccurrences(of: "]", with: "", options: .literal, range: nil)
                
                if string2.contains(", ") == true {
                    favArray = string2.components(separatedBy: ", ")
                    print("data...")
                    //print(favArray)
                    dataHandler.saveObject(item: favArray, saveName: "favJobsArray")
                }else{
                    print("Empty Array")
                }
                
            } else {
                dataHandler.saveObject(item: [], saveName: "favJobsArray")
                print("No such Favs exists!.")
            }
        })
        
        return favArray
        
    }
    
    class func checkFavs(jobID: String)-> Bool {
        var favArray = dataHandler.getStringArray(saveName: "favJobsArray")
        var favFound = false
        if favArray.isEmpty == false{
            for i in 0 ..< favArray.count  {
                if favArray[i] == jobID{
                    favFound = true
                }
            }
        }
        
        return favFound
        
    }
    
    class func removeAJob(jobID: String){
        
        
        let ID = FIRAuth.auth()?.currentUser?.uid ?? "fail"
        
        var favString = ""
        var favArray = [String]()
        
        FIRDatabase.database().reference().root.child("users").child("\(FIRAuth.auth()!.currentUser!.uid)").child("favJobs").observeSingleEvent(of: .value, with: { snapshot in
            if let snapDict = snapshot.value as? [String:AnyObject]{
                favString = snapDict["favJobs"] as! String
                
                //Convert String to String Array
                let string1 = favString.replacingOccurrences(of: "[", with: "", options: .literal, range: nil)
                let string2 = string1.replacingOccurrences(of: "]", with: "", options: .literal, range: nil)
                
                favArray = string2.components(separatedBy: ", ")
                
                favArray = favArray.filter() {$0 != jobID}
                
                let intArray = favArray.map { Int($0)!}
                let dataArray : [String : AnyObject] = [
                    "favJobs" : String(describing: intArray) as AnyObject]
                
                let dbRef = FIRDatabase.database().reference()
                dbRef.child("users").child(ID).child("favJobs").setValue(dataArray)
                print("Sending Data")
                dataHandler.saveObject(item: favArray, saveName: "favJobsArray")
                
            }
        })
        
        
    }
    
    
    class func viewedAJob(jobID: String){
        
        
        let ID = FIRAuth.auth()?.currentUser?.uid ?? "fail"
        
        
        var viewedString = ""
        var viewedArray = [String]()
        
        FIRDatabase.database().reference().root.child("users").child("\(FIRAuth.auth()!.currentUser!.uid)").child("viewedJobs").observeSingleEvent(of: .value, with: { snapshot in
            if let snapDict = snapshot.value as? [String:AnyObject]{
                viewedString = snapDict["viewedJobs"] as! String
                
                //Convert String to String Array
                let string1 = viewedString.replacingOccurrences(of: "[", with: "", options: .literal, range: nil)
                let string2 = string1.replacingOccurrences(of: "]", with: "", options: .literal, range: nil)
                
                if string2.contains(", ") == true {
                    viewedArray = string2.components(separatedBy: ", ")
                    viewedArray.append(jobID)
                    
                }else{
                    
                    if (dataHandler.ContainsNumbers(s: string2).count > 0)
                    {
                        viewedArray = string2.components(separatedBy: ", ")
                        viewedArray.append(jobID)
                    }else{
                        viewedArray.removeAll()
                        viewedArray.append(jobID)
                    }
                }
                
                let intArray = viewedArray.map { Int($0)!}
                let dataArray : [String : AnyObject] = [
                    "viewedJobs" : String(describing: intArray) as AnyObject]
                
                let dbRef = FIRDatabase.database().reference()
                dbRef.child("users").child(ID).child("viewedJobs").setValue(dataArray)
                print("Sending Data")
                dataHandler.saveObject(item: viewedArray, saveName: "viewedJobsArray")
                
                
            } else {
                viewedArray.append(jobID)
                let intArray = viewedArray.map { Int($0)!}
                let dataArray : [String : AnyObject] = [
                    "viewedJobs" : String(describing: intArray) as AnyObject]
                
                let dbRef = FIRDatabase.database().reference()
                dbRef.child("users").child(ID).child("viewedJobs").setValue(dataArray)
                print("Sending Data")
                dataHandler.saveObject(item: viewedArray, saveName: "viewedJobsArray")
                
            }
        })
        
        
    }
    
    class func getViewedJobs(){
        
        var favString = ""
        var viewedArray = [String]()
        
        FIRDatabase.database().reference().root.child("users").child("\(FIRAuth.auth()!.currentUser!.uid)").child("viewedJobs").observeSingleEvent(of: .value, with: { snapshot in
            if let snapDict = snapshot.value as? [String:AnyObject]{
                favString = snapDict["viewedJobs"] as! String
                
                //Convert String to String Array
                let string1 = favString.replacingOccurrences(of: "[", with: "", options: .literal, range: nil)
                let string2 = string1.replacingOccurrences(of: "]", with: "", options: .literal, range: nil)
                
                if string2.contains(", ") == true {
                    viewedArray = string2.components(separatedBy: ", ")
                    print("data...")
                    //print(viewedArray)
                    dataHandler.saveObject(item: viewedArray, saveName: "viewedJobsArray")
                }else{
                    dataHandler.saveObject(item: [], saveName: "viewedJobsArray")
                    print("Empty Array")
                }
                
            } else {
                dataHandler.saveObject(item: [], saveName: "favJobsArray")
                print("No such viewd jobs exists!.")
            }
        })
        
        
    }
    
    class func checkViewed(jobID: String)-> Bool {
        var viewedArray = dataHandler.getStringArray(saveName: "viewedJobsArray")
        var viewFound = false
        if viewedArray.isEmpty == false{
            for i in 0 ..< viewedArray.count  {
                if viewedArray[i] == jobID{
                    viewFound = true
                }
            }
        }
        
        return viewFound
        
    }
    
    
    class func getReedKey(){
        
        print("getting Key")
        
        var api_key = ""
        
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil {
                FIRDatabase.database().reference().root.child("api_keys").observeSingleEvent(of: .value, with: { snapshot in
                    if let snapDict = snapshot.value as? [String:AnyObject]{
                        api_key = snapDict["reed_Key"] as! String
                        dataHandler.saveObject(item: snapDict["reed_Key"] as! String, saveName: "reed_Key")
                        print("api_key from firebase...")
                    } else {
                        print("No such api_Key exists!.")
                    }
                })
            }
        }
        
        dataHandler.saveObject(item: api_key, saveName: "reed_Key")
        
    }
    
    class func ContainsNumbers(s: String) -> [Character]
    {
        return s.characters.filter { ("0"..."9").contains($0)}
    }
    
}

//
//  ProfileViewController.swift
//  Job View
//
//  Created by Joshua Gill on 06/04/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseCore
import UIKit
import CoreLocation

class ProfileViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate, EasyTipViewDelegate {
    
    var locationManager:CLLocationManager!
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var currentCity = "London"
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var signOutButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var locationField: UITextField!
    @IBOutlet weak var roleTitle: UILabel!
    @IBOutlet weak var loggedInUser_lbl: UILabel!
    
    @IBOutlet weak var jobviews_lbl: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Open the Menu
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil {
                self.load()
            } else {
                EasyTipView.show(forView: self.menuButton, withinSuperview: self.menuButton.superview, text: "Log in to access your profile from any device.")
                self.load()
                self.signOutButton.isHidden = true
                self.deleteButton.isHidden = true
            }
        }
        
        //SetUpTips
        setUpTip()
        
    }
    
    func load(){

        //Get location
        self.locationField.text = dataHandler.getString(saveName: "localLocation")
        
        //Get Role
        if dataHandler.getString(saveName: "localJobRole") == "IT"{
            //Ask user to login or complete questionaire
            self.loggedInUser_lbl.text = "Not logged in"
            roleTitle.text = dataHandler.getString(saveName: "localJobRole")
            
            
        }else{
            //show user's current role
            roleTitle.text = dataHandler.getString(saveName: "localJobRole")
            
            //Get number of viewed jobs
            let viewedArray = dataHandler.getStringArray(saveName: "viewedJobsArray")
            jobviews_lbl.text = String(viewedArray.count)
        }
        
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil {
                //See if the user is logged in via twitter
                if dataHandler.getString(saveName: "twitter") != ""{
                    //Set the display name to the twitter name
                    self.loggedInUser_lbl.text = dataHandler.getString(saveName: "twitter")
                }
                else{
                    //Set the dispplay name to the user's email address
                    self.loggedInUser_lbl.text = FIRAuth.auth()?.currentUser?.email
                }
            } else {
                //If the user is not logged in, load the data using the defaults
                self.loggedInUser_lbl.text = "Not logged in"
            }
        }
    }
    
    func logout(){
        //Sign out
        try! FIRAuth.auth()!.signOut()
        
        //Alert the user
        self.alert(title: "Authentication", message: "You have been logged out.")
        dataHandler.resetLocalDetails()
        self.performSegue(withIdentifier: "profileToHome", sender: self)

    }
    
    func delete(){
        
        print(FIRAuth.auth()!.currentUser!.uid)
        
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil {
                FIRDatabase.database().reference().root.child("users").child("\(FIRAuth.auth()!.currentUser!.uid)").removeValue { (error, ref) in
                    if error != nil {
                        self.alert(title: "Authentication", message: String(describing: error?.localizedDescription))
                        print("error \(String(describing: error?.localizedDescription))")
                    }else{
                        print("user data deleted")
                        
                        FIRAuth.auth()?.currentUser?.delete { error in
                            if let error = error {
                                self.alert(title: "Authentication", message: String(describing: error.localizedDescription))
                                print(error.localizedDescription)
                            } else {
                                //Alert the user
                                self.alert(title: "Authentication", message: "Your account has been deleted.")
                                dataHandler.resetLocalDetails()
                                print("Done")
                            }
                        }
                    }
                }
            }
            
        }

        
        
        

    }
    
    //Open the side menu from the menu button
    @IBAction func openMenu(_ sender: Any) {
        self.revealViewController().revealToggle(sender)
    }
    
    @IBAction func saveLocation(_ sender: Any) {
        setLocation(location: locationField.text!)
        locationField.endEditing(true)
        
    }
    @IBAction func getLocation(_ sender: Any) {
        determineMyCurrentLocation()
        
    }

    @IBAction func logoutButton(_ sender: Any) {
        logout()
    }
    
    @IBAction func deleteAccountButton(_ sender: Any) {
        checkForDeletion()
    }
    
    func alertHome(title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.performSegue(withIdentifier: "profileToHome", sender: self)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func alert(title:String,message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func checkForDeletion(){
        let optionMenuController = UIAlertController(title: nil, message: "Are you sure you want to delete your account?", preferredStyle: .actionSheet)
        
        // Create UIAlertAction for UIAlertController
        
        let deleteAction = UIAlertAction(title: "Delete my account", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.delete()
            self.performSegue(withIdentifier: "profileToHome", sender: self)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Canceled")
        })
        
        // Add UIAlertAction in UIAlertController
        optionMenuController.addAction(deleteAction)
        optionMenuController.addAction(cancelAction)
        
        // Present UIAlertController with Action Sheet
        
        self.present(optionMenuController, animated: true, completion: nil)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        manager.stopUpdatingLocation()
        latitude = userLocation.coordinate.latitude
        longitude = userLocation.coordinate.longitude
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        getCity()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    
    func getCity(){
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: latitude, longitude: longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            // City
            if let city = placeMark.addressDictionary!["City"] as? NSString {
                print(city)
                print(city)
                self.currentCity = city as String
                self.setLocation(location: self.currentCity)
                self.locationField.text = self.currentCity
            }
            
            
        })
    }
    
    func setLocation(location:String){
        
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil {
                
                let ID = FIRAuth.auth()?.currentUser?.uid ?? "failed to get ID"
                
                let dataArray : [String : AnyObject] = [
                    "location" : location as AnyObject]
                
                let dbRef = FIRDatabase.database().reference()
                dbRef.child("users").child(ID).updateChildValues(dataArray)
                print("Sending Data")
            }else{
                self.alert(title: "Account", message: "Log in to access your profile from any device.")
            }
        }
        dataHandler.saveObject(item: location, saveName: "localLocation")
        alert(title: "Location Update", message: "Your preferred location has been updated.")
    }
    
    //Show Tip
    func setUpTip(){
        
        var preferences = EasyTipView.Preferences()
        preferences.drawing.foregroundColor = UIColor.black
        preferences.drawing.backgroundColor = UIColor.white
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.right
        preferences.animating.showInitialAlpha = 0
        
        EasyTipView.globalPreferences = preferences
        
    }
    
    //Dismiss Tip
    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        print("\(tipView) did dismiss!")
    }
}


  

//
//  AllRolesViewController.swift
//  Job View
//
//  Created by Joshua Gill on 18/03/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseCore
import UIKit
import CoreLocation

class AllRolesViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    
    @IBOutlet weak var roleTitle: UILabel!
    @IBOutlet weak var roleDescription: UITextView!
    @IBOutlet weak var pickerView: UIPickerView!
    
    var roleDetailsArray = [roles]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        roleDetailsArray = roles.getRoles()
        loadDetails(row: 0)
        
        //Open the Menu
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        
        roleDescription.textContainerInset = UIEdgeInsetsMake(10,10,10,10)
        roleDescription.layer.cornerRadius = 8
        
    }
    
    
    @IBAction func openMenu(_ sender: Any) {
        self.revealViewController().revealToggle(sender)
    }
    
    @IBAction func SetTheRoleButton(_ sender: Any) {
        setRole(role: roleTitle.text!)
        alert(title: "Your Job Role", message: "Your job role has been updated to: " + roleTitle.text!)
    }
    @IBAction func searchForJobsButton(_ sender: Any) {
        performSegue(withIdentifier: "allToSearch", sender: self)
    }
    func loadDetails(row: Int){
        roleTitle.text = roleDetailsArray[row].roleTitle
        roleDescription.text = roleDetailsArray[row].roleDescription
        
    }
    
    func setRole(role:String){
        
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil {
                
                let ID = FIRAuth.auth()?.currentUser?.uid ?? "failed to get ID"
                
                let dataArray : [String : AnyObject] = [
                    "role" : role as AnyObject]
                
                let dbRef = FIRDatabase.database().reference()
                dbRef.child("users").child(ID).updateChildValues(dataArray)
                print("Sending Data")
            }else{
                self.alert(title: "Account", message: "Log in to access your profile from any device.")
            }
        }
        dataHandler.saveObject(item: role, saveName: "localJobRole")
    }
    
    // DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return roleDetailsArray.count
    }
    
    // Delegate
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = roleDetailsArray[row].roleTitle
        let myTitle = NSAttributedString(string: titleData, attributes: [NSForegroundColorAttributeName : UIColor.white])
        return myTitle
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        loadDetails(row: row)
    }
    
    func alert(title:String,message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "allToSearch"{
            let myVC = segue.destination as? SearchViewController
            myVC?.searchString = roleTitle.text!
            navigationController?.pushViewController(myVC!, animated: true)
        }
    }
    
    
}

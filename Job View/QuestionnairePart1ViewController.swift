//
//  QuestionnairePart1ViewController.swift
//  Job View
//
//  Created by Joshua Gill on 17/02/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import Foundation
import UIKit

class QuestionnairePart1ViewController: UIViewController {
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    @IBOutlet weak var questionnaireDescriptionScrl: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Open the Menu
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.backgroundImageView.addBlurEffect()
        
        questionnaireDescriptionScrl.text = "You are part of a project to launch a Student Union app. The app will allow students to: \n - Book tickets for events \n - Collect food and drinks promotions \n - Find out about clubs and societies \n - Complete surveys and enter \n - Competitions \n - Research local area \n \nAnswer the questions using this brief to help find your ideal job role. \n\nBy clicking next, you agree that you understand that the result of this questionnaire is only a suggestion."
        
    }
    
    @IBAction func openMenu(_ sender: Any) {
        self.revealViewController().revealToggle(sender)
    }
    
}



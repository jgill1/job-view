//
//  JobDetailsViewController.swift
//  Job View
//
//  Created by Joshua Gill on 14/02/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SafariServices
import MapKit
import CoreLocation
import Firebase
import FirebaseAuth
import FirebaseDatabase

class JobDetailsViewController: UIViewController, CLLocationManagerDelegate {

    
    @IBOutlet weak var location_lbl: UILabel!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var job_lbl: UILabel!
    @IBOutlet weak var employer_lbl: UILabel!
    @IBOutlet weak var description_txtVw: UITextView!
    @IBOutlet weak var glassdoorWebsite: UIButton!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var applications_lbl: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var fav_Icon: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var favJobButton: UIButton!
    @IBOutlet weak var salary_lbl: UILabel!
    @IBOutlet weak var glassdoorLogo: UIImageView!
    @IBOutlet weak var gdRating_lbl: UILabel!
    @IBOutlet weak var gdRating_background: UILabel!
    
    typealias JSONStandard = [String : AnyObject]
    var companyURL = "www.google.co.uk"
    var glassdoorWebsiteURL = "www.google.co.uk"
    var locationManager:CLLocationManager!
    var job: jobs!
    var currentPickerRow: Int?
    var searchString: String?
    var glassdoorHeadline = ""
    var glassdoorPro = ""
    var glassdoorCon = ""
    var glassdoorRating = "'"
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(searchString!)
        
        //Load Data
        job_lbl.text=job.jobTitle
        employer_lbl.text=job.employerName
        //clear up description text
        var jobDescription = job.jobDescription.replacingOccurrences(of: "&#163;", with: "£")
        jobDescription = jobDescription.replacingOccurrences(of: "&#43;", with: "+")
        jobDescription = jobDescription.replacingOccurrences(of: "<p>", with: "")
        jobDescription = jobDescription.replacingOccurrences(of: "</p>", with: "\n")
        jobDescription = jobDescription.replacingOccurrences(of: "</br>", with: "\n")
        jobDescription = jobDescription.replacingOccurrences(of: "<br />", with: "\n")
        jobDescription = jobDescription.replacingOccurrences(of: "<br>", with: "")
        jobDescription = jobDescription.replacingOccurrences(of: "<strong>", with: "")
        jobDescription = jobDescription.replacingOccurrences(of: "</strong>", with: "")
        jobDescription = jobDescription.replacingOccurrences(of: "&amp;", with: "&")
        jobDescription = jobDescription.replacingOccurrences(of: "<li>", with: "")
        jobDescription = jobDescription.replacingOccurrences(of: "</li>", with: "\n")
        jobDescription = jobDescription.replacingOccurrences(of: "<ul>", with: "")
        jobDescription = jobDescription.replacingOccurrences(of: "</ul>", with: "\n")
   
        description_txtVw.text = jobDescription
        applications_lbl.text = ("Applications: " + String(job.applications))
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        let minSalary = job.minSalary as NSNumber
        let maxSalary = job.maxSalary as NSNumber
        
        salary_lbl.text = "    Salary: " + formatter.string(from: minSalary)! + " to " + formatter.string(from: maxSalary)!
        
        loadMap()
        determineMyCurrentLocation()
        
        
        print(job.logoURL)
        
        
        //Download Logo and set to logo imageview and background
        logoImageView.sd_setImage(with: URL(string: job.logoURL), placeholderImage: nil, options: [.continueInBackground, .progressiveDownload])
        logoImageView.layer.cornerRadius = 8.0
        logoImageView.clipsToBounds = true
        
        self.backgroundImageView.sd_setImage(with: URL(string: job.logoURL), placeholderImage: nil, options: [.continueInBackground, .progressiveDownload])
        self.backgroundImageView.addBlurEffect()
        
        
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil {
                dataHandler.viewedAJob(jobID: self.job.jobID)
                if dataHandler.checkFavs(jobID: String(self.job.jobID)) == true{
                    self.fav_Icon.isHidden = false
                }else{
                    self.fav_Icon.isHidden = true
                    
                }
                
            } else {
                self.fav_Icon.isHidden = true
            }
        }
        
        
        
        //Set up button Styles
        glassdoorWebsite.layer.borderWidth = 2
        glassdoorWebsite.layer.borderColor = UIColor.white.cgColor
        glassdoorWebsite.titleEdgeInsets = UIEdgeInsetsMake(10,10,10,10)
        glassdoorWebsite.isHidden = true
        gdRating_lbl.isHidden = true
        glassdoorLogo.isHidden = true
        gdRating_background.isHidden = true
        description_txtVw.textContainerInset = UIEdgeInsetsMake(10,10,10,10)
        description_txtVw.layer.cornerRadius = 8
        gdRating_background.layer.cornerRadius = 8
        gdRating_background.layer.masksToBounds = true
        
        requestGlassDoorData()
        
        //Open the Menu
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())


    }
    
    @IBAction func openMenu(_ sender: Any) {
        self.revealViewController().revealToggle(sender)
    }
    
    func loadMap(){
        let address = self.job.jobLocation + ", uk" //"1 Infinite Loop, CA, USA"
        location_lbl.text = address
        let geocoder = CLGeocoder()
        
        geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
            if((error) != nil){
                print("Error", error!)
            }
            if let placemark = placemarks?.first {
                //Set Pin Details
                let annotation = MKPointAnnotation()
                annotation.coordinate = placemark.location!.coordinate
                annotation.title = self.job.employerName
                
                //Add Pin to map
                self.map.addAnnotation(annotation)
                
                let span = MKCoordinateSpanMake(0.075, 0.075)
                let region = MKCoordinateRegion(center: placemark.location!.coordinate, span: span)
                self.map.setRegion(region, animated: true)
                
            }
        })
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
            map.showAnnotations(map.annotations, animated: true)
        }
    }
    
    @IBAction func apply(_ sender: Any) {
        if let url = URL(string: job.jobUrl) {
            let vc = SFSafariViewController(url: url)
            present(vc, animated: true)
        }
    }
    
    
    
    @IBAction func companyWebsite(_ sender: Any) {
        
        if let url = URL(string: "http://" + companyURL) {
            let vc = SFSafariViewController(url: url)
            present(vc, animated: true)
        }
    }
    
    @IBAction func linkedInWebsite(_ sender: Any) {
        if let url = URL(string: job.linkedIn) {
            let vc = SFSafariViewController(url: url)
            present(vc, animated: true)
        }
    }
    @IBAction func glassDoorWebsite(_ sender: Any) {
            performSegue(withIdentifier: "detailsToGlassdoor", sender: self)
        
    }
    @IBAction func favJobButtonPressed(_ sender: Any) {
        
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil {
                if dataHandler.checkFavs(jobID: String(self.job.jobID)) == false{
                    dataHandler.favAJob(jobID: String(self.job.jobID))
                    self.fav_Icon.isHidden = false
                    self.favJobButton.setTitle("Remove Favourite", for: .normal)
                }else{
                    dataHandler.removeAJob(jobID: String(self.job.jobID))
                    self.fav_Icon.isHidden = true
                    self.favJobButton.setTitle("Favourite", for: .normal)

                }
                
            } else {
                self.alert(title: "Favourites", message: "Please login to save your favourites.")
            }
        }
        
        
    }
    
    func requestGlassDoorData(){
        let URL = "http://api.glassdoor.com/api/api.htm?"
        
        let parameters: Parameters = [
            "t.p": "118822",
            "t.k": "iSwIAkxYcI",
            "format": "json",
            "l": self.job.jobLocation,
            "action": "employers",
            "q": self.job.employerName
        ]
        //print("Making Request")
        
        Alamofire.request(URL, parameters: parameters).responseJSON(completionHandler: {
            response in
            //print("Request made")
            //print(response.data!)
            self.parseData(JSONData: response.data!)
            
        })
    }
    
    func parseData(JSONData : Data){
        do{
            let readableJSON = try JSONSerialization.jsonObject(with: JSONData, options: .mutableContainers) as! JSONStandard
            //print(readableJSON)
            
            if let response = readableJSON["response"] as? JSONStandard{
                if var employers = response["employers"] as? [JSONStandard]{
                    if employers.isEmpty == false{
                        let employer = employers[0] 
                        //print(employer)
                        
                        //Collect data from JSON
                        glassdoorRating = employer["overallRating"] as! String
                        companyURL = employer["website"] as! String
                        //print(companyURL)
                        
                        //print(employerName)
                        if glassdoorRating == "0"{
                        }else{
                            gdRating_lbl.text = "Rating: " + glassdoorRating
                            gdRating_lbl.isHidden = false
                            glassdoorLogo.isHidden = false
                            gdRating_background.isHidden = false
                        }
                        
                        //print("finding Glassdoor URL")
                        if var featuredReview = employer["featuredReview"] as? JSONStandard{
                            glassdoorWebsiteURL = featuredReview["attributionURL"] as! String
                            glassdoorHeadline = featuredReview["headline"] as! String
                            glassdoorPro = featuredReview["pros"] as! String
                            glassdoorCon = featuredReview["cons"] as! String
                            
                            glassdoorWebsite.isHidden = false
                            
                        }
                    }
                }
            }
            
        }
        catch{
            print(error)
        }
    }
    
    func alert(title:String,message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let _:CLLocation = locations[0] as CLLocation
        
        map.showAnnotations(map.annotations, animated: true)
        
        manager.stopUpdatingLocation()
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    @IBAction func goToSearch(_ sender: Any) {
        performSegue(withIdentifier: "detailsToSearch", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detailsToSearch"{
            let myVC = segue.destination as? SearchViewController
            myVC?.searchString = searchString!
            myVC?.currentPickerRow = currentPickerRow!
            navigationController?.pushViewController(myVC!, animated: true)
        }else if segue.identifier == "detailsToGlassdoor"{
            let myVC = segue.destination as? GlassdoorViewController
            myVC?.logoURL = job.logoURL
            myVC?.glassdoorHeadline = glassdoorHeadline
            myVC?.glassdoorPro = glassdoorPro
            myVC?.glassdoorCon = glassdoorCon
            myVC?.glassdoorURL = glassdoorWebsiteURL
            myVC?.glassdoorRating = glassdoorRating
            navigationController?.popToViewController(myVC!, animated: true)
        }
    }

}



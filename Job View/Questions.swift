//
//  Questions.swift
//  Job View
//
//  Created by Joshua Gill on 17/02/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import Foundation

class questions {
    var answerOne: String
    var answerTwo: String
    var A1projectManager : Int
    var A1productionSupport: Int
    var A1businessAnalyst: Int
    var A1softwareTester: Int
    var A1softwareDeveloper: Int
    var A1technicalSales: Int
    var A2projectManager : Int
    var A2productionSupport: Int
    var A2businessAnalyst: Int
    var A2softwareTester: Int
    var A2softwareDeveloper: Int
    var A2technicalSales: Int
    
    
    
    init(answerOne: String, answerTwo: String, A1projectManager : Int, A1productionSupport: Int, A1businessAnalyst: Int, A1softwareTester: Int, A1softwareDeveloper: Int, A1technicalSales: Int, A2projectManager : Int, A2productionSupport: Int, A2businessAnalyst: Int, A2softwareTester: Int, A2softwareDeveloper: Int, A2technicalSales: Int) {
        self.answerOne = answerOne
        self.answerTwo = answerTwo
        self.A1projectManager = A1projectManager
        self.A1productionSupport = A1productionSupport
        self.A1businessAnalyst = A1businessAnalyst
        self.A1softwareTester = A1softwareTester
        self.A1softwareDeveloper = A1softwareDeveloper
        self.A1technicalSales = A1technicalSales
        self.A2projectManager = A2projectManager
        self.A2productionSupport = A2productionSupport
        self.A2businessAnalyst = A2businessAnalyst
        self.A2softwareTester = A2softwareTester
        self.A2softwareDeveloper = A2softwareDeveloper
        self.A2technicalSales = A2technicalSales
        
    }
    
    class func getQuestions()->[questions]{
        var arrayOfQuestions = [questions]()
        
        arrayOfQuestions = [
            questions(answerOne: "I enjoy working with a mix of other students", answerTwo: "I like working with people I know", A1projectManager: 2, A1productionSupport: 0, A1businessAnalyst: 3, A1softwareTester: 0, A1softwareDeveloper: 0, A1technicalSales: 1, A2projectManager: 0, A2productionSupport: 1, A2businessAnalyst: 0, A2softwareTester: 1, A2softwareDeveloper: 2, A2technicalSales: 0),
            questions(answerOne: "I will produce the new ideas for the project", answerTwo: "I will use and perfect current ideas", A1projectManager: 1, A1productionSupport: 0, A1businessAnalyst: 3, A1softwareTester: 0, A1softwareDeveloper: 2, A1technicalSales: 2, A2projectManager: 0, A2productionSupport: 1, A2businessAnalyst: 0, A2softwareTester: 1, A2softwareDeveloper: 0, A2technicalSales: 0),
            questions(answerOne: "I am adaptable and creative", answerTwo: "I am structured and analytical", A1projectManager: 0, A1productionSupport: 1, A1businessAnalyst: 2, A1softwareTester: 0, A1softwareDeveloper: 3, A1technicalSales: 1, A2projectManager: 0, A2productionSupport: 3, A2businessAnalyst: 2, A2softwareTester: 3, A2softwareDeveloper: 0, A2technicalSales: 0),
            questions(answerOne: "I like to know how the app will affect the students", answerTwo: "I like to know how the app will work", A1projectManager: 1, A1productionSupport: 0, A1businessAnalyst: 2, A1softwareTester: 0, A1softwareDeveloper: 0, A1technicalSales: 3, A2projectManager: 0, A2productionSupport: 2, A2businessAnalyst: 0, A2softwareTester: 2, A2softwareDeveloper: 3, A2technicalSales: 0),
            questions(answerOne: "I enjoy working on different things at once", answerTwo: "I like focusing on a specific task", A1projectManager: 3, A1productionSupport: 1, A1businessAnalyst: 2, A1softwareTester: 0, A1softwareDeveloper: 0, A1technicalSales: 3, A2projectManager: 0, A2productionSupport: 0, A2businessAnalyst: 0, A2softwareTester: 2, A2softwareDeveloper: 3, A2technicalSales: 1),
            questions(answerOne: "I'm interested in what the students want from the app", answerTwo: "I'm interested in the project's needs", A1projectManager: 0, A1productionSupport: 0, A1businessAnalyst: 1, A1softwareTester: 0, A1softwareDeveloper: 0, A1technicalSales: 3, A2projectManager: 3, A2productionSupport: 0, A2businessAnalyst: 0, A2softwareTester: 3, A2softwareDeveloper: 2, A2technicalSales: 0),
            questions(answerOne: "I like being involved in the early stages of the project", answerTwo: "I like being involved after the project launch", A1projectManager: 3, A1productionSupport: 0, A1businessAnalyst: 2, A1softwareTester: 1, A1softwareDeveloper: 1, A1technicalSales: 0, A2projectManager: 0, A2productionSupport: 2, A2businessAnalyst: 0, A2softwareTester: 0, A2softwareDeveloper: 0, A2technicalSales: 3),
            questions(answerOne: "I like to work in a quiet space where I can focus", answerTwo: "I prefer to be in a busy atmosphere", A1projectManager: 1, A1productionSupport: 1, A1businessAnalyst: 0, A1softwareTester: 3, A1softwareDeveloper: 2, A1technicalSales: 0, A2projectManager: 2, A2productionSupport: 3, A2businessAnalyst: 3, A2softwareTester: 0, A2softwareDeveloper: 2, A2technicalSales: 3),
            questions(answerOne: "I like to have a routine", answerTwo: "I like doing different things everyday", A1projectManager: 0, A1productionSupport: 0, A1businessAnalyst: 0, A1softwareTester: 3, A1softwareDeveloper: 2, A1technicalSales: 0, A2projectManager: 2, A2productionSupport: 3, A2businessAnalyst: 2, A2softwareTester: 0, A2softwareDeveloper: 0, A2technicalSales: 2)
        ]
        return arrayOfQuestions
    }
    
    
}


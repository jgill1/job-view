//
//  QuestionnairePart3ViewController.swift
//  Job View
//
//  Created by Joshua Gill on 17/02/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import Foundation
import UIKit
import Charts

class QuestionnairePart3ViewController: UIViewController, ChartViewDelegate, EasyTipViewDelegate {
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var questionBox: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var answerOneButton: UIButton!
    @IBOutlet weak var answerTwoButton: UIButton!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var progressBar: UIProgressView!
    
    var questionsArray = [questions]()
    var projectManager: Int?
    var productionSupport: Int?
    var businessAnalyst: Int?
    var softwareTester: Int?
    var softwareDeveloper: Int?
    var technicalSales: Int?
    var projectManagerPart2: Int?
    var productionSupportPart2: Int?
    var businessAnalystPart2: Int?
    var softwareTesterPart2: Int?
    var softwareDeveloperPart2: Int?
    var technicalSalesPart2: Int?
    var questionCount = 0
    var jobRoles = [String]()
    var values = [Double]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        projectManagerPart2 = projectManager
        productionSupportPart2 = productionSupport
        businessAnalystPart2 = businessAnalyst
        softwareTesterPart2 = softwareTester
        softwareDeveloperPart2 = softwareDeveloper
        technicalSalesPart2 = technicalSales
        
        //Show Chart Tip
        EasyTipView.show(forView: self.questionBox, withinSuperview: self.questionBox.superview,
                         text: "The chart below shows you your current progress to allow you to see how your choices influence your potential job role. \n-Tap to Dismiss-")
        
        //Get Jobs
        jobRoles = ["Software Tester", "Software Developer", "Technical Support", "Business Analyst", "Technical Sales", "Project Manager"]
        updateValues()
        setChart(dataPoints: jobRoles, values: values)
        
        questionsArray = questions.getQuestions()
        progressBar.progress=0
        updateQuestions()
        doneButton.isHidden=true
        //Open the Menu
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        print(projectManager!,productionSupport!,businessAnalyst!,softwareTester!,softwareDeveloper!)
        
        //Set up button Styles
        answerOneButton.layer.borderWidth = 2
        answerOneButton.layer.borderColor = UIColor.white.cgColor
        answerOneButton.titleEdgeInsets = UIEdgeInsetsMake(10,10,10,10)
        
        answerTwoButton.layer.borderWidth = 2
        answerTwoButton.layer.borderColor = UIColor.white.cgColor
        answerTwoButton.titleEdgeInsets = UIEdgeInsetsMake(10,10,10,10)
        
        self.backgroundImageView.addBlurEffect()
        
    }
    
    func updateValues(){
        values.removeAll()
        values = [Double(softwareTester!), Double(softwareDeveloper!), Double(productionSupport!), Double(businessAnalyst!), Double(technicalSales!), Double(projectManager!)]
        
    }
    
    func setChart(dataPoints: [String], values: [Double]){

        pieChartView.delegate = self
        pieChartView.clear()
        
        var dataEntries: [PieChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let entry = PieChartDataEntry()
            entry.y = values[i]
            entry.label = dataPoints[i]
            dataEntries.append(entry)
            
        }
        
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "Roles")
        
        var colors: [UIColor] = []
        
        
        for i in 0..<dataPoints.count {
            let red = Double(42*i)
            let green = Double(42*i)
            let blue = Double(42*i)
            
            let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 0.95)
            colors.append(color)
            
            pieChartDataSet.colors = colors
            pieChartDataSet.selectionShift = 7
            pieChartDataSet.drawValuesEnabled = false
            
        }
        
        var dataSets = [PieChartDataSet]()
        
        dataSets.append(pieChartDataSet)
        
        let pieChartData = PieChartData(dataSets: dataSets)
        
        pieChartView.delegate = self;
        pieChartView.usePercentValuesEnabled = false
        pieChartView.holeColor = UIColor.clear
        pieChartView.holeRadiusPercent = 0.7
        pieChartView.transparentCircleRadiusPercent = 0.6
        pieChartView.drawCenterTextEnabled = true
        
        pieChartView.drawHoleEnabled = true
        pieChartView.rotationAngle = 0.0
        pieChartView.rotationEnabled = true
        pieChartView.legend.enabled = false
        
        pieChartView.data = pieChartData
        
        
    }
    
    @IBAction func openMenu(_ sender: Any) {
        self.revealViewController().revealToggle(sender)
    }
    
    //Show Tip
    func setUpTip(){
        
        var preferences = EasyTipView.Preferences()
        preferences.drawing.foregroundColor = UIColor.black
        preferences.drawing.backgroundColor = UIColor.white
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.bottom
        preferences.animating.showInitialAlpha = 0
        preferences.animating.showDuration = 1.5
        
        EasyTipView.globalPreferences = preferences
        
    }
    
    //Dismiss Tip
    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        print("\(tipView) did dismiss!")
    }
    
    @IBAction func answerOnePressed(_ sender: Any) {
        questionCount += 1
        switch questionCount {
        case 1:
            updatePointsAnswerOne()
            updateQuestions()
        case 2:
            updatePointsAnswerOne()
            updateQuestions()
        case 3:
            updatePointsAnswerOne()
            updateQuestions()
        case 4:
            updatePointsAnswerOne()
            updateQuestions()
        case 5:
            updatePointsAnswerOne()
            updateQuestions()
        case 6:
            updatePointsAnswerOne()
            updateQuestions()
        case 7:
            updatePointsAnswerOne()
            updateQuestions()
        case 8:
            updatePointsAnswerOne()
            updateQuestions()
        case 9:
            progressBar.progress=1.0
            doneButton.isHidden=false
            answerOneButton.isEnabled = false
            answerTwoButton.isEnabled = false
            answerOneButton.backgroundColor = UIColor.black
            answerTwoButton.backgroundColor = UIColor.black
        default:
            break
        }
        
    }
    
    @IBAction func answerTwoPressed(_ sender: Any) {
        questionCount += 1
        switch questionCount {
        case 1:
            updatePointsAnswerTwo()
            updateQuestions()
        case 2:
            updatePointsAnswerTwo()
            updateQuestions()
        case 3:
            updatePointsAnswerTwo()
            updateQuestions()
        case 4:
            updatePointsAnswerTwo()
            updateQuestions()
        case 5:
            updatePointsAnswerTwo()
            updateQuestions()
        case 6:
            updatePointsAnswerTwo()
            updateQuestions()
        case 7:
            updatePointsAnswerTwo()
            updateQuestions()
        case 8:
            updatePointsAnswerTwo()
            updateQuestions()
        case 9:
            progressBar.progress=1.0
            doneButton.isHidden=false
            answerOneButton.isEnabled = false
            answerTwoButton.isEnabled = false
            answerOneButton.backgroundColor = UIColor.black
            answerTwoButton.backgroundColor = UIColor.black
        default:
            break
        }
        
    }
    
    func updateQuestions() {
        answerOneButton.setTitle(questionsArray[questionCount].answerOne, for: .normal)
        answerTwoButton.setTitle(questionsArray[questionCount].answerTwo, for: .normal)
        progressBar.progress=progressBar.progress+0.1
        
        updateValues()
        setChart(dataPoints: jobRoles, values: values)
    }
    
    func updatePointsAnswerOne(){
        
        projectManager = projectManager!+questionsArray[questionCount-1].A1projectManager
        productionSupport = productionSupport!+questionsArray[questionCount-1].A1productionSupport
        businessAnalyst = businessAnalyst!+questionsArray[questionCount-1].A1businessAnalyst
        softwareTester = softwareTester!+questionsArray[questionCount-1].A1softwareTester
        softwareDeveloper = softwareDeveloper!+questionsArray[questionCount-1].A1softwareDeveloper
        technicalSales = technicalSales!+questionsArray[questionCount-1].A1technicalSales
        
    }
    
    func updatePointsAnswerTwo(){
        projectManager = projectManager!+questionsArray[questionCount-1].A2projectManager
        productionSupport = productionSupport!+questionsArray[questionCount-1].A2productionSupport
        businessAnalyst = businessAnalyst!+questionsArray[questionCount-1].A2businessAnalyst
        softwareTester = softwareTester!+questionsArray[questionCount-1].A2softwareTester
        softwareDeveloper = softwareDeveloper!+questionsArray[questionCount-1].A2softwareDeveloper
        technicalSales = technicalSales!+questionsArray[questionCount-1].A2technicalSales
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        back()
    }
    
    func back(){
        if questionCount != 0{
            questionCount = 0
            
            
            projectManager = projectManagerPart2
            productionSupport = productionSupportPart2
            businessAnalyst = businessAnalystPart2
            softwareTester = softwareTesterPart2
            softwareDeveloper = softwareDeveloperPart2
            technicalSales = technicalSalesPart2
            updateQuestions()
            progressBar.progress=0
            
        }else{
           performSegue(withIdentifier: "threeToTwo", sender: self)
        }
        
    }
    


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueTwo"{
            let myVC = segue.destination as? ResultsViewController
            myVC?.projectManager = projectManager!
            myVC?.productionSupport=productionSupport!
            myVC?.businessAnalyst=businessAnalyst!
            myVC?.softwareDeveloper=softwareDeveloper!
            myVC?.softwareTester=softwareTester!
            myVC?.technicalSales=technicalSales!
            myVC?.transfer = true
            navigationController?.pushViewController(myVC!, animated: true)
        }
    }
}


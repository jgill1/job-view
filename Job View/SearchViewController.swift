//
//  SearchViewController.swift
//  Job View
//
//  Created by Joshua Gill on 14/02/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import Alamofire
import MapKit
import CoreLocation
import SwiftSpinner

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, CLLocationManagerDelegate, EasyTipViewDelegate {
    
    //Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var currentLocationButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var locationTextView: UITextField!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    //Variables
    typealias JSONStandard = [String : AnyObject]
    var locationManager:CLLocationManager!
    let sortArray = ["Select A Filter...", "Newest", "Days Left", "Applications", "Min Salary Desc", "Max Salary Asc"]
    var jobsArray = [jobs]()
    var row = 0
    var filterShown = false
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var currentCity = "London"
    var currentPickerRow = 0
    var searchString = ""
    var location = "'"
    var searchMade = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //Set the table view background to transparent
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.separatorColor = UIColor.clear
        
        
        //See if any data was sent over
        if searchString == ""{
            //If the search string is nil, get the user's set job role
            setPlaceholder()
        }else{
            //If the data was sent over, use that
            searchField.text = searchString
            pickerView.selectRow(currentPickerRow, inComponent: 0, animated: true)
            startSearch()
        }
        
        //Open the Menu
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        //Set the location to the user's default location
        locationTextView.text = dataHandler.getString(saveName: "localLocation")
        
        //Format the location button
        currentLocationButton.layer.borderWidth = 2
        currentLocationButton.layer.borderColor = UIColor.white.cgColor
        currentLocationButton.titleEdgeInsets = UIEdgeInsetsMake(10,10,10,10)
        
        //set up the table to show an empty table message
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        

    }
    
    //Show Tip
    func setUpTip(){
        
        //Set preferences
        var preferences = EasyTipView.Preferences()
        preferences.drawing.foregroundColor = UIColor.black
        preferences.drawing.backgroundColor = UIColor.white
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.any
        preferences.animating.showInitialAlpha = 0
        
        EasyTipView.globalPreferences = preferences
        
        
    }
    
    //Dismiss Tip
    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        print("\(tipView) did dismiss!")
    }
    
    //Set the searchfield's placeholder text
    func setPlaceholder(){
        self.searchField.text = dataHandler.getString(saveName: "localJobRole")
        
        if searchField.text == "IT"{
            self.setUpTip()
            EasyTipView.show(forView: self.filterButton, withinSuperview: self.filterButton.superview, text: "Click here to change your search location and filter the results. \n-Tap to Dismiss-")
        }
    }
    
    //Start the seach when the search button is pressed
    @IBAction func search(_ sender: Any) {
        startSearch()
    }
    
    //Clear the table delegate when the class is dropped
    deinit {
        self.tableView.emptyDataSetSource = nil
        self.tableView.emptyDataSetDelegate = nil
    }
    
    func startSearch(){
        
        //load data
        location = dataHandler.getString(saveName: "localLocation")
        let locationFieldText = locationTextView.text!
        
        //Check if the location has changed
        if location == ""{
            location = "London"
        }
        
        if location != locationFieldText{
            location = locationFieldText
        }
        
        requestJobData()
        self.tableView.reloadData()
        
        //Hide the filters
        let point = CGPoint(x: 0,y :0)
        scrollView.setContentOffset(point,animated: true)
        filterShown = false
        //Sort the array
        sortArray(row: currentPickerRow)
        
        if searchMade == false{
            searchMade = true
            //Blur Background
            self.backgroundImageView.addBlurEffect()
        }
        
        
    }
    
    //Get job details
    func requestJobData(){
        
        //Show activity indicator
        SwiftSpinner.show("Searching for jobs...")
        
        //Load details
        let URL = "https://www.reed.co.uk/api/1.0/search?"
        let keywordsText = searchField.text

        
        //Load Parameters
        let parameters: Parameters = [
            "keywords": keywordsText!,
            "locationName": location,
            "distanceFromLocation": "100",
            "graduate": "true"
            ]
        
        //Load Security Details
        let user = "927d498d-8736-499a-bf97-7d3244996129"
        let password = ""
        
        //Make Request
        Alamofire.request(URL, parameters :parameters).authenticate(user: user, password: password).responseJSON(completionHandler: {
            response in
            self.parseData(JSONData: response.data!)
            
        })
    }
    
    //Parse Data
    func parseData(JSONData : Data){
        jobsArray.removeAll()
        do {
            guard let teamJSON =  try JSONSerialization.jsonObject(with: JSONData, options: []) as? [String: Any],
                let jobJSONData = teamJSON["results"] as? [[String: Any]]
                else { return }
            
            //looping through all the json objects in the array teams
            for i in 0 ..< jobJSONData.count{
                let jobID = jobJSONData[i]["jobId"] as! Int
                let jobTitle = (jobJSONData[i]["jobTitle"] as! NSString) as String
                let applications = jobJSONData[i]["applications"] as! Int
                let employerName = jobJSONData[i]["employerName"] as! String
                let locationName = jobJSONData[i]["locationName"] as! String
                let jobDescription = jobJSONData[i]["jobDescription"] as! String
                let jobUrl = jobJSONData[i]["jobUrl"] as! String
                
                //Get Dates
                let expirationDateString = jobJSONData[i]["expirationDate"] as! String
                let addedDateString = jobJSONData[i]["date"] as! String
                
                //Convert Dates
                let inputFormatter = DateFormatter()
                inputFormatter.dateFormat = "dd/MM/yyyy"
                let expirationDate = inputFormatter.date(from: expirationDateString)! as NSDate
                let addedDate = inputFormatter.date(from: addedDateString)! as NSDate
                
                //Get and check salaries
                var maxSalary = 0
                if jobJSONData[i]["maximumSalary"] as? Int == nil{
                    
                }else{
                    maxSalary = jobJSONData[i]["maximumSalary"] as! Int
                }
                
                var minSalary = 0
                if jobJSONData[i]["minimumSalary"] as? Int == nil{
                    
                }else{
                    minSalary = jobJSONData[i]["minimumSalary"] as! Int
                }
                
                //get linkedin data
                var logoURL = ""
                let URL = "https://www.linkedin.com/ta/federator?"
                let parameters: Parameters = [
                    "query": employerName,
                    "types": "company"
                ]
                
                //make request
                Alamofire.request(URL, parameters :parameters).responseJSON(completionHandler: {
                    response in
                    do{
                        let readableLogoJSON = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! JSONStandard
                        //print(readableLogoJSON)
                        if let companies = readableLogoJSON["company"] as? JSONStandard{
                            if var results = companies["resultList"] as? [JSONStandard]{
                                //print(results)
                                if results.isEmpty == false {
                                    let result = results[0]
                                    if result["imageUrl"] as? String != nil{
                                        logoURL = result["imageUrl"] as! String
                                        let linkedIn = result["url"] as! String
                                        //load data into the array
                                        self.jobsArray.append(jobs.init(jobID: String(jobID), jobTitle: jobTitle, employerName: employerName, jobLocation: locationName, jobDescription: jobDescription, applications: applications , jobUrl: jobUrl, minSalary: minSalary, maxSalary: maxSalary, logoURL: logoURL, linkedIn: linkedIn, dateEnd: expirationDate, dateAdded: addedDate))
                                        self.sortArray(row: self.currentPickerRow)
                                    }
                                }
                            }
                        }
                    }
                    catch{
                        //Load data into the array if linkedin api fails
                        self.jobsArray.append(jobs.init(jobID: String(jobID), jobTitle: jobTitle, employerName: employerName, jobLocation: locationName, jobDescription: jobDescription, applications: applications , jobUrl: jobUrl, minSalary: minSalary, maxSalary: maxSalary, logoURL: logoURL, linkedIn: "", dateEnd: expirationDate, dateAdded: addedDate))
                        //Alert the user as to why their is missing data
                        self.alert(title: "LINKEDIN API", message: "API Error")
                        print(error)
                    }
                })
            }
        } catch {
            //Alert the user to failure
            alert(title: "REED API", message: "API Error")
            print(error)
        }
        
        //Stop the activity icon
        SwiftSpinner.hide()
    }
    
    //Get the table size
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jobsArray.count
    }
    
    //Load data onto the table
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("JobTableViewCell", owner: self, options: nil)?.first as! JobTableViewCell
        
        //Download Logo
        cell.logo_image.sd_setImage(with: URL(string: jobsArray[indexPath.row].logoURL), placeholderImage: nil, options: [.continueInBackground, .progressiveDownload])
        cell.logo_image.layer.cornerRadius = 8.0
        cell.logo_image.clipsToBounds = true
        
        //load label data
        cell.location_lbl.text = jobsArray[indexPath.row].jobLocation
        cell.title_lbl.text=jobsArray[indexPath.row].jobTitle
        
        //Load application data
        cell.applications_lbl.text=String(jobsArray[indexPath.row].applications)
        
        //Check if for fav and viewed
        if dataHandler.checkFavs(jobID: jobsArray[indexPath.row].jobID) == true {
            print(dataHandler.checkFavs(jobID: jobsArray[indexPath.row].jobID))
            cell.sideColour_lbl.backgroundColor = UIColor.orange
        }else if dataHandler.checkViewed(jobID: jobsArray[indexPath.row].jobID) == true{
            cell.sideColour_lbl.backgroundColor = UIColor.blue
        }
        
        if jobsArray[indexPath.row].applications < 20{
            cell.applications_lbl.textColor = UIColor.green
        }else if jobsArray[indexPath.row].applications < 50{
            cell.applications_lbl.textColor = UIColor.orange
        }else{
            cell.applications_lbl.textColor = UIColor.red
        }
        
        //Load salary data
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        
        let minSalary = jobsArray[indexPath.row].minSalary as NSNumber
        let maxSalary = jobsArray[indexPath.row].maxSalary as NSNumber
        cell.salary_lbl.text = formatter.string(from: minSalary)! + " to " + formatter.string(from: maxSalary)!
        
        //Get Remaining Days
        let date1 = jobsArray[indexPath.row].dateEnd as Date
        let date2 = Date()
        
        //Calculate days left
        cell.daysLeft_lbl.text = "Days Left: " + String(daysBetweenDates(startDate: date2, endDate: date1))
        
        //Set the cell background colour
        cell.background_lbl.layer.masksToBounds = true
        cell.background_lbl.layer.cornerRadius = 5
        cell.sideColour_lbl.layer.masksToBounds = true
        cell.sideColour_lbl.roundCorners(corners: [.topLeft, .bottomLeft], radius: 5)
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    
    //Set cell height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
        
    }
    
    //Go to job details view when a row is selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected...")
        row = indexPath.row
        
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "segueFour", sender: self)
        
    }
    
    //Add the favirotes button to the table
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let favorite = UITableViewRowAction(style: .normal, title: "Favorite") { action, index in
            
            //Check if the user is logged in
            FIRAuth.auth()?.addStateDidChangeListener { auth, user in
                if user != nil {
                    if dataHandler.checkFavs(jobID: String(self.jobsArray[indexPath.row].jobID)) == false{
                        //Add the job
                        dataHandler.favAJob(jobID: String(self.jobsArray[indexPath.row].jobID))
                        let indexPath = IndexPath(item: indexPath.row, section: 0)
                        tableView.reloadRows(at: [indexPath], with: .fade)
                        let currentCell = tableView.cellForRow(at: indexPath) as! JobTableViewCell
                        currentCell.sideColour_lbl.backgroundColor = UIColor.orange
                        
                    }else{
                        //Remove the job
                        dataHandler.removeAJob(jobID: String(self.jobsArray[indexPath.row].jobID))
                        let indexPath = IndexPath(item: indexPath.row, section: 0)
                        tableView.reloadRows(at: [indexPath], with: .fade)
                        let currentCell = tableView.cellForRow(at: indexPath) as! JobTableViewCell
                        currentCell.sideColour_lbl.backgroundColor = UIColor.clear
                        
                    }
                    
                } else {
                    //Alert the user to login to add to their favourites
                    self.alert(title: "Favourites", message: "Please login to save your favourites.")
                }
            }
        }
        //Set icon background colour
        favorite.backgroundColor = UIColor.orange
        
        return [ favorite ]
    }
    
    //Send detials over to the next view contoller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueFour"{
            let myVC = segue.destination as? JobDetailsViewController
            myVC?.job = jobsArray[row]
            myVC?.searchString = searchField.text!
            myVC?.currentPickerRow = currentPickerRow
            navigationController?.pushViewController(myVC!, animated: true)
        }
    }
    
    //hide empty cells
    override func viewWillAppear(_ animated: Bool) {
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    //Open menu funciton
    @IBAction func openMenu(_ sender: Any) {
        self.revealViewController().revealToggle(sender)
    }
    
    //hide keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        startSearch()
        return true
    }
    
    //Show alert funciton
    func alert(title:String,message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //Show the filters menu
    @IBAction func filterButtonPressed(_ sender: Any) {
        
        if filterShown == true{
            let point = CGPoint(x: 0,y :0)
            scrollView.setContentOffset(point,animated: true)
            filterShown = false
            filterButton.setTitleColor(UIColor.white, for: UIControlState.normal)
            
        }else{
            let point = CGPoint(x: 0,y :-122)
            scrollView.setContentOffset(point,animated: true)
            filterShown = true
            filterButton.setTitleColor(UIColor.black, for: UIControlState.normal)
        }
    }
    
    //Get user's current location button
    @IBAction func currentLocation(_ sender: Any) {
        determineMyCurrentLocation()
    }

    //get the users location
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    //store the users location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        manager.stopUpdatingLocation()
        latitude = userLocation.coordinate.latitude
        longitude = userLocation.coordinate.longitude
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        getCity()
    }
    
    //location error handling
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    //get the user's city using glgeocoder
    func getCity(){
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: latitude, longitude: longitude)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            // City
            if let city = placeMark.addressDictionary!["City"] as? NSString {
                print(city)
                print(city)
                self.currentCity = city as String
                self.locationTextView.text = self.currentCity
                self.requestJobData()
                self.tableView.reloadData()
            }
            
            
        })
    }
    
    // DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sortArray.count
    }
    
    // Delegate
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        //Set titles for the picker view and change the colour to white
        let titleData = sortArray[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSForegroundColorAttributeName : UIColor.white])
        return myTitle
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        currentPickerRow = row
        sortArray(row: row)
    }
    
    //Calculate the difference between two dates
    func daysBetweenDates(startDate: Date, endDate: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([Calendar.Component.day], from: startDate, to: endDate)
        return components.day!
    }
    
    func sortArray (row: Int){
        //["Ending Date", "Applications", "Min Salary", "Max Salary"]
        switch row {
        case 0:
            //Default
            self.tableView.reloadData()
        case 1:
            //Newest
            self.jobsArray.sort(by: { $1.dateAdded.compare($0.dateAdded as Date) == ComparisonResult.orderedAscending })
            self.tableView.reloadData()
        case 2:
            //Days Left
            self.jobsArray.sort(by: { $0.dateEnd.compare($1.dateEnd as Date) == ComparisonResult.orderedAscending })
            self.tableView.reloadData()
        case 3:
            //Applcations
            self.jobsArray.sort { $0.applications < $1.applications }
            self.tableView.reloadData()
        case 4:
            //Min Salary
            self.jobsArray.sort { $0.minSalary < $1.minSalary }
            self.tableView.reloadData()
        case 5:
            //Max Salary
            self.jobsArray.sort { $1.maxSalary < $0.maxSalary }
            self.tableView.reloadData()
        default:
            break
        }
    }
    
}



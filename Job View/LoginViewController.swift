//
//  LoginViewController.swift
//  Job View
//
//  Created by Joshua Gill on 14/02/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import Foundation
import Alamofire
import FirebaseAuth
import Firebase
import FirebaseDatabase
import Fabric
import TwitterKit
import SwiftSpinner

class LoginViewController: UIViewController, UITextFieldDelegate, EasyTipViewDelegate {
    
    //Outlets
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var blur_background: UIImageView!
    @IBOutlet weak var loggedIn_lbl: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Open menu gesture
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        //Add the twitter button to the view controller
        setupTwitterButton()
        
        loggedIn_lbl.isHidden = true
        blur_background.isHidden = true
        blur_background.addBlurEffect()
        
        //Set Up Tips
        setUpTip()
        
        
    }
    
    //Load the twitter button and set up it's functionality
    func setupTwitterButton() {
        
        //Attempt login
        let twitterButton = TWTRLogInButton { (session, error) in
            if let err = error {
                self.alert(title: "Twitter Login", message: "Failed to login")
                print("Failed to login via Twitter: ", err)
                return
            }
            
            //Get twitter authencation data
            guard let token = session?.authToken else { return }
            guard let secret = session?.authTokenSecret else { return }
            let credentials = FIRTwitterAuthProvider.credential(withToken: token, secret: secret)
            
            FIRAuth.auth()?.signIn(with: credentials, completion: { (user, error) in
                
                if let err = error {
                    self.alert(title: "Twitter Login", message: "Failed to login to Firebase with Twitter: ")
                    print("Failed to login to Firebase with Twitter: ", err)
                    return
                }
                
                FIRAuth.auth()?.addStateDidChangeListener { auth, user in
                    if user != nil {
                        FIRDatabase.database().reference().root.child("users").child("\(FIRAuth.auth()!.currentUser!.uid)").observeSingleEvent(of: .value, with: { snapshot in
                            if let snapDict = snapshot.value as? [String:AnyObject]{
                                _ = snapDict["role"] as! String
                                //Get data from firebase and set locally
                                
                                dataHandler.getRole()
                                dataHandler.getLocation()
                                dataHandler.getViewedJobs()
                                _ = dataHandler.getFavJobs()
                            } else {
                                let ID = FIRAuth.auth()?.currentUser?.uid ?? "fail"
                                
                                //Create a JSON with the data to be sent to the server
                                let dataArray : [String : AnyObject] = [
                                    "role" : "IT" as AnyObject,
                                    "location" : "London" as AnyObject]
                                
                                //Send the data to firebase
                                let dbRef = FIRDatabase.database().reference()
                                dbRef.child("users").child(ID).setValue(dataArray)
                                print("Sending Data")
                            }
                        })
                    }
                }
                
                dataHandler.saveObject(item: session?.userName as Any, saveName: "twitter")
                dataHandler.saveObject(item: "LoggedIn", saveName: "True")
                

                
                //Inform the user they have logged in
                self.alert(title: "Twitter Login", message: "Welcome!")
                self.blur_background.isHidden = false
                self.loggedIn_lbl.isHidden = false
                //Take the user to the homepage
                self.revealViewController().revealToggle(nil)
                
            })
        }
        
        
        //Add the twitter button to the view controller
        self.view.addSubview(twitterButton)
        twitterButton.frame = CGRect(x: 36, y: 590, width: 304, height: 41)
    }

    //Create the user
    @IBAction func Create(_ sender: Any) {
        
        password.endEditing(true)
        email.endEditing(true)
        
        if (email.text?.contains("@"))! == true{
            if (password.text?.characters.count)!>6{
                
                FIRAuth.auth()?.createUser(withEmail: email.text!, password: password.text!, completion: {
                    user, error in
                    
                    if error != nil{
                        //If and account already exists, log the user in
                        self.login()
                    }
                    else{
                        //Create the account and log the user in
                        print("Account Created")
                        self.alert(title: "Authentication", message: "Account Created")
                        self.login()
                        self.createBasicProfile()
                    }
                })
            }
            else{
                EasyTipView.show(forView: self.loginButton, withinSuperview: self.loginButton.superview, text: "Please ensure your password has more than 6 characters.")
            }
            
        }else{
            EasyTipView.show(forView: self.loginButton, withinSuperview: self.loginButton.superview, text: "Please ensure you enter a valid email address")
        }

    }
    
    func login(){
        //Log the user in
        FIRAuth.auth()?.signIn(withEmail: email.text!, password: password.text!, completion: {
            user, error in
            
            if error != nil{
                print("Incorrect")
                //Alert the user
                self.alert(title: "Authentication", message: "Incorrect Login Details")
            }
            else{
                //Log the user in and get data from the firebase server
                print("Login")
                
                //Alert the user to the fact they have logged in
                self.alert(title: "Login", message: "Welcome!")
                self.blur_background.isHidden = false
                self.loggedIn_lbl.isHidden = false
                print(FIRAuth.auth()?.currentUser?.uid ?? "fail")
                dataHandler.getRole()
                dataHandler.getLocation()
                dataHandler.saveObject(item: "", saveName: "twitter")
                dataHandler.saveObject(item: "LoggedIn", saveName: "True")
                dataHandler.getViewedJobs()
                _ = dataHandler.getFavJobs()
                
                //Take the user to the home page
                self.revealViewController().revealToggle(nil)
            }
        })
    }

    //Create a basic profile with the default data for new accounts
    func createBasicProfile(){
        
        let ID = FIRAuth.auth()?.currentUser?.uid ?? "fail"
        
        //Create a JSON with the data to be sent to the server
        let dataArray : [String : AnyObject] = [
            "role" : "IT" as AnyObject,
            "location" : "London" as AnyObject]
        
        //Send the data to firebase
        let dbRef = FIRDatabase.database().reference()
        dbRef.child("users").child(ID).setValue(dataArray)
        print("Sending Data")
        
        
        
    }
    
    //Show Tip
    func setUpTip(){
        
        var preferences = EasyTipView.Preferences()
        preferences.drawing.foregroundColor = UIColor.black
        preferences.drawing.backgroundColor = UIColor.white
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top
        preferences.animating.showInitialAlpha = 0
        EasyTipView.globalPreferences = preferences
        
    }
    
    //Dismiss Tip
    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        print("\(tipView) did dismiss!")
    }
    
    //Open the side menu via the button
    @IBAction func openMenu(_ sender: Any) {
        self.revealViewController().revealToggle(sender)
    }
    
    //Scroll the page up with the text box is active
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let point = CGPoint(x: 0,y :150)
        scrollView.setContentOffset(point,animated: true)
    }
    
    //Hide the keyboared when the user is finished
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //Move the page back to normal when edditing is done
    func textFieldDidEndEditing(_ textField: UITextField) {
        let point = CGPoint(x: 0,y :0)
        scrollView.setContentOffset(point,animated: true)
        Create(self)
    }
    
    //Alert Function
    func alert(title:String,message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

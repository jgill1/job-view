//
//  FavouritesViewController.swift
//  Job View
//
//  Created by Joshua Gill on 04/03/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//


import UIKit
import Firebase
import FirebaseAuth
import Alamofire
import SwiftSpinner

class FavouritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!

    typealias JSONStandard = [String : AnyObject]
    
    var jobsArray = [jobs]()
    var selectedRow = 0
    var favArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Set the table view background to transparent
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.separatorColor = UIColor.clear
        
        //set up the table
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        //Open the Menu
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil {
                self.loadFavs()
            } else {
                self.alert(title: "Favourites", message: "Please login to save your favourites.")
                SwiftSpinner.hide()
            }
        }
        

        
    }
    

    
    func loadFavs(){
        
        SwiftSpinner.show("Finding your favourites...")
        var favJobs = ""
        
        FIRDatabase.database().reference().root.child("users").child("\(FIRAuth.auth()!.currentUser!.uid)").child("favJobs").observeSingleEvent(of: .value, with: { snapshot in
            if let snapDict = snapshot.value as? [String:AnyObject]{
                print("getting jobs")
                favJobs = snapDict["favJobs"] as! String
                print(favJobs)
                
                //Convert String to String Array
                let string1 = favJobs.replacingOccurrences(of: "[", with: "", options: .literal, range: nil)
                let string2 = string1.replacingOccurrences(of: "]", with: "", options: .literal, range: nil)
                
                if string2.contains(", ") == false && self.ContainsNumbers(s: string2).count == 0{
                    print("Empty Array")
                }else{
                    self.favArray = string2.components(separatedBy: ", ")
                    print(self.favArray)
                    print(self.favArray.count)
                    dataHandler.saveObject(item: self.favArray, saveName: "favJobsArray")
                    if self.favArray.isEmpty == false{
                        for i in 0 ..< self.favArray.count  {
                            //print(self.testArray.first!)
                            self.requestJobData(row: i)
                        }
                    }
                    
                }
                
            } else {
                print("No such Favs exists!.")
                SwiftSpinner.hide()
            }
        })
        SwiftSpinner.hide()
    }
    

    func requestJobData(row : Int){
        
        print("getting data")
        
        
        let URL = "http://www.reed.co.uk/api/1.0/jobs/" + favArray[row].trimmingCharacters(in: ["\"", "[", "\\", "]"])
        
        
        let user = "927d498d-8736-499a-bf97-7d3244996129"
        let password = ""
        
        
        Alamofire.request(URL).authenticate(user: user, password: password).responseJSON(completionHandler: {
            response in
            self.parseData(JSONData: response.data!)
            
        })
    }
    
    func parseData(JSONData : Data){
        do {
            //print("getting more data")
            let readableJSON = try JSONSerialization.jsonObject(with: JSONData, options: .mutableContainers) as! JSONStandard
            //print(readableJSON)
            
            if readableJSON["jobId"] as? Int != nil{
                let jobID = readableJSON["jobId"] as! Int
                let jobTitle = (readableJSON["jobTitle"] as! NSString) as String
                let applications = readableJSON["applicationCount"] as! Int
                let employerName = readableJSON["employerName"] as! String
                let locationName = readableJSON["locationName"] as! String
                let jobDescription = readableJSON["jobDescription"] as! String
                let jobUrl = readableJSON["jobUrl"] as! String
                
                let expirationDateString = readableJSON["expirationDate"] as! String
                let addedDateString = readableJSON["datePosted"] as! String
                
                let inputFormatter = DateFormatter()
                inputFormatter.dateFormat = "dd/MM/yyyy"
                let expirationDate = inputFormatter.date(from: expirationDateString)! as NSDate
                let addedDate = inputFormatter.date(from: addedDateString)! as NSDate

                
                var maxSalary = 0
                if readableJSON["maximumSalary"] as? Int == nil{
                    
                }else{
                    maxSalary = readableJSON["maximumSalary"] as! Int
                }
                
                var minSalary = 0
                if readableJSON["minimumSalary"] as? Int == nil{
                    
                }else{
                    minSalary = readableJSON["minimumSalary"] as! Int
                }
                
                var logoURL = ""
                let URL = "https://www.linkedin.com/ta/federator?"
                let parameters: Parameters = [
                    "query": employerName,
                    "types": "company"
                ]
                Alamofire.request(URL, parameters :parameters).responseJSON(completionHandler: {
                    response in
                    do{
                        let readableLogoJSON = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! JSONStandard
                        //print(readableLogoJSON)
                        if let companies = readableLogoJSON["company"] as? JSONStandard{
                            if var results = companies["resultList"] as? [JSONStandard]{
                                //print(results)
                                if results.isEmpty == false {
                                    let result = results[0]
                                    if result["imageUrl"] as? String != nil{
                                        logoURL = result["imageUrl"] as! String
                                        let linkedIn = result["url"] as! String
                                        self.jobsArray.append(jobs.init(jobID: String(jobID), jobTitle: jobTitle, employerName: employerName, jobLocation: locationName, jobDescription: jobDescription, applications: applications , jobUrl: jobUrl, minSalary: minSalary, maxSalary: maxSalary, logoURL: logoURL, linkedIn: linkedIn, dateEnd: expirationDate, dateAdded: addedDate))
                                        self.tableView.reloadData()

                                    }
                                }
                            }
                        }
                    }
                    catch{
                        self.jobsArray.append(jobs.init(jobID: String(jobID), jobTitle: jobTitle, employerName: employerName, jobLocation: locationName, jobDescription: jobDescription, applications: applications , jobUrl: jobUrl, minSalary: minSalary, maxSalary: maxSalary, logoURL: logoURL, linkedIn: "", dateEnd: expirationDate, dateAdded: addedDate))
                        self.tableView.reloadData()
                        self.alert(title: "LINKEDIN API", message: "API Error")
                        print(error)
                    }
                })
            }
            
        } catch {
            alert(title: "REED API", message: "API Error")
            print(error)
        }
        SwiftSpinner.hide()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jobsArray.count
    }
    
    //Load data onto the table
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("JobTableViewCell", owner: self, options: nil)?.first as! JobTableViewCell
        
        //Download Logo
        cell.logo_image.sd_setImage(with: URL(string: jobsArray[indexPath.row].logoURL), placeholderImage: nil, options: [.continueInBackground, .progressiveDownload])
        cell.logo_image.layer.cornerRadius = 8.0
        cell.logo_image.clipsToBounds = true
        
        cell.location_lbl.text = jobsArray[indexPath.row].jobLocation
        cell.title_lbl.text=jobsArray[indexPath.row].jobTitle
        cell.applications_lbl.text=String(jobsArray[indexPath.row].applications)
        
        if jobsArray[indexPath.row].applications < 20{
            cell.applications_lbl.textColor = UIColor.green
        }else if jobsArray[indexPath.row].applications < 50{
            cell.applications_lbl.textColor = UIColor.orange
        }else{
            cell.applications_lbl.textColor = UIColor.red
        }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        
        let minSalary = jobsArray[indexPath.row].minSalary as NSNumber
        let maxSalary = jobsArray[indexPath.row].maxSalary as NSNumber
        cell.salary_lbl.text = formatter.string(from: minSalary)! + " to " + formatter.string(from: maxSalary)!
        
        //Get Remaining Days
        let date1 = jobsArray[indexPath.row].dateEnd as Date
        let date2 = Date()
        
        
        cell.daysLeft_lbl.text = "Days Left: " + String(daysBetweenDates(startDate: date2, endDate: date1))
        
        //Set the cell background colour
        cell.background_lbl.layer.masksToBounds = true
        cell.background_lbl.layer.cornerRadius = 5
        cell.sideColour_lbl.layer.masksToBounds = true
        cell.sideColour_lbl.roundCorners(corners: [.topLeft, .bottomLeft], radius: 5)
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected...")
        selectedRow = indexPath.row
        
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "segueSix", sender: self)
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let favorite = UITableViewRowAction(style: .normal, title: "Unfavorite") { action, index in
            FIRAuth.auth()?.addStateDidChangeListener { auth, user in
                if user != nil {
                    
                    dataHandler.removeAJob(jobID: String(self.jobsArray[indexPath.row].jobID))
                    self.jobsArray.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                    self.tableView.reloadData()
                    
                } else {
                    self.alert(title: "Favourites", message: "Please login to save your favourites.")
                }
            }
        }
        favorite.backgroundColor = UIColor.orange
        
        return [ favorite ]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueSix"{
            let myVC = segue.destination as? JobDetailsViewController
            myVC?.job = jobsArray[selectedRow]
            myVC?.searchString = dataHandler.getString(saveName: "localJobRole")
            myVC?.currentPickerRow = 0
            navigationController?.pushViewController(myVC!, animated: true)
        }
    }
    
    //Clear the table delegate when the class is dropped
    deinit {
        self.tableView.emptyDataSetSource = nil
        self.tableView.emptyDataSetDelegate = nil
    }
    
    //hide empty cells
    override func viewWillAppear(_ animated: Bool) {
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    @IBAction func openMenu(_ sender: Any) {
        self.revealViewController().revealToggle(sender)
    }
    
    func alert(title:String,message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func daysBetweenDates(startDate: Date, endDate: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([Calendar.Component.day], from: startDate, to: endDate)
        return components.day!
    }
    
    func ContainsNumbers(s: String) -> [Character]
    {
        return s.characters.filter { ("0"..."9").contains($0)}
    }
}


//
//  jobs.swift
//  Job View
//
//  Created by Joshua Gill on 14/02/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import Foundation

class jobs : NSObject, NSCoding {
    var jobID: String
    var jobTitle: String
    var employerName : String
    var jobLocation: String
    var jobDescription: String
    var applications: Int
    var jobUrl: String
    var minSalary: Int
    var maxSalary: Int
    var logoURL: String
    var linkedIn: String
    var dateEnd: NSDate
    var dateAdded: NSDate
    
    
    init(jobID: String, jobTitle: String, employerName : String, jobLocation: String, jobDescription: String, applications: Int, jobUrl: String, minSalary: Int, maxSalary: Int, logoURL: String, linkedIn: String, dateEnd: NSDate, dateAdded: NSDate) {
        self.jobID = jobID
        self.jobTitle = jobTitle
        self.employerName = employerName
        self.jobLocation = jobLocation
        self.jobDescription = jobDescription
        self.applications = applications
        self.jobUrl = jobUrl
        self.minSalary = minSalary
        self.maxSalary = maxSalary
        self.logoURL = logoURL
        self.linkedIn = linkedIn
        self.dateEnd = dateEnd
        self.dateAdded = dateAdded
        
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let jobID = aDecoder.decodeObject(forKey: "jobID") as! String
        let jobTitle = aDecoder.decodeObject(forKey: "jobTitle") as! String
        let employerName = aDecoder.decodeObject(forKey: "employerName") as! String
        let jobLocation = aDecoder.decodeObject(forKey: "jobLocation") as! String
        let jobDescription = aDecoder.decodeObject(forKey: "jobDescription") as! String
        let applications = aDecoder.decodeInteger(forKey: "applications")
        let jobUrl = aDecoder.decodeObject(forKey: "jobUrl") as! String
        let minSalary = aDecoder.decodeInteger(forKey: "minSalary")
        let maxSalary = aDecoder.decodeInteger(forKey: "maxSalary")
        let logoURL = aDecoder.decodeObject(forKey: "logoURL") as! String
        let linkedIn = aDecoder.decodeObject(forKey: "linkedIn") as! String
        let dateEnd = aDecoder.decodeObject(forKey: "dateEnd") as! NSDate
        let dateAdded = aDecoder.decodeObject(forKey: "dateAdded") as! NSDate
        self.init(jobID: jobID, jobTitle: jobTitle, employerName : employerName, jobLocation: jobLocation, jobDescription: jobDescription, applications: applications, jobUrl: jobUrl, minSalary: minSalary, maxSalary: maxSalary, logoURL: logoURL, linkedIn: linkedIn, dateEnd: dateEnd, dateAdded: dateAdded)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(jobID, forKey: "jobID")
        aCoder.encode(jobTitle, forKey: "jobTitle")
        aCoder.encode(employerName, forKey: "employerName")
        aCoder.encode(jobLocation, forKey: "jobLocation")
        aCoder.encode(jobDescription, forKey: "jobDescription")
        aCoder.encode(applications, forKey: "applications")
        aCoder.encode(jobUrl, forKey: "jobUrl")
        aCoder.encode(minSalary, forKey: "minSalary")
        aCoder.encode(maxSalary, forKey: "maxSalary")
        aCoder.encode(logoURL, forKey: "logoURL")
        aCoder.encode(linkedIn, forKey: "linkedIn")
        aCoder.encode(dateEnd, forKey: "dateEnd")
        aCoder.encode(dateAdded, forKey: "dateAdded")
    }
    
}

//
//  QuestionnairePart2ViewController.swift
//  Job View
//
//  Created by Joshua Gill on 17/02/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import Foundation
import UIKit

class QuestionnairePart2ViewController: UIViewController {
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var questionBox: UILabel!
    
    @IBOutlet weak var answerOneButton: UIButton!
    @IBOutlet weak var answerTwoButton: UIButton!
    @IBOutlet weak var answerThreeButton: UIButton!
    @IBOutlet weak var answerFourButton: UIButton!
    @IBOutlet weak var answerFiveButton: UIButton!
    @IBOutlet weak var answerSixButton: UIButton!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    var projectManager = 1
    var productionSupport = 1
    var businessAnalyst = 1
    var softwareTester = 1
    var softwareDeveloper = 1
    var technicalSales = 1
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Open the Menu
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.backgroundImageView.addBlurEffect()
        
        //Set up button Styles
        answerOneButton.layer.borderWidth = 2
        answerOneButton.layer.borderColor = UIColor.white.cgColor
        answerOneButton.titleEdgeInsets = UIEdgeInsetsMake(10,10,10,10)
        
        answerTwoButton.layer.borderWidth = 2
        answerTwoButton.layer.borderColor = UIColor.white.cgColor
        answerTwoButton.titleEdgeInsets = UIEdgeInsetsMake(10,10,10,10)
        
        answerThreeButton.layer.borderWidth = 2
        answerThreeButton.layer.borderColor = UIColor.white.cgColor
        answerThreeButton.titleEdgeInsets = UIEdgeInsetsMake(10,10,10,10)
        
        answerFourButton.layer.borderWidth = 2
        answerFourButton.layer.borderColor = UIColor.white.cgColor
        answerFourButton.titleEdgeInsets = UIEdgeInsetsMake(10,10,10,10)
        
        answerFiveButton.layer.borderWidth = 2
        answerFiveButton.layer.borderColor = UIColor.white.cgColor
        answerFiveButton.titleEdgeInsets = UIEdgeInsetsMake(10,10,10,10)
        
        answerSixButton.layer.borderWidth = 2
        answerSixButton.layer.borderColor = UIColor.white.cgColor
        answerSixButton.titleEdgeInsets = UIEdgeInsetsMake(10,10,10,10)
        
    }
    
    @IBAction func openMenu(_ sender: Any) {
        self.revealViewController().revealToggle(sender)
    }
    
    @IBAction func answerOneButton(_ sender: Any) {
        if answerOneButton.backgroundColor == UIColor.black {
            businessAnalyst=businessAnalyst-3
            answerOneButton.backgroundColor = UIColor.clear
        }
        else {
            answerOneButton.backgroundColor = UIColor.black
            businessAnalyst=businessAnalyst+3
        }
    }
    
    @IBAction func answerTwoButton(_ sender: Any) {
        if answerTwoButton.backgroundColor == UIColor.black {
            answerTwoButton.backgroundColor = UIColor.clear
            projectManager=projectManager-3
        }
        else {
            answerTwoButton.backgroundColor = UIColor.black
            projectManager=projectManager+3
        }
    }
    
    @IBAction func answerThreeButton(_ sender: Any) {
        if answerThreeButton.backgroundColor == UIColor.black {
            answerThreeButton.backgroundColor = UIColor.clear
            softwareTester=softwareTester-3
        }
        else {
            answerThreeButton.backgroundColor = UIColor.black
            softwareTester=softwareTester+3
        }
    }
    
    @IBAction func answerFourButton(_ sender: Any) {
        if answerFourButton.backgroundColor == UIColor.black {
            answerFourButton.backgroundColor = UIColor.clear
            productionSupport=productionSupport-3
        }
        else {
            answerFourButton.backgroundColor = UIColor.black
            productionSupport=productionSupport+3
        }
    }
    @IBAction func answerFiveButton(_ sender: Any) {
        if answerFiveButton.backgroundColor == UIColor.black {
            answerFiveButton.backgroundColor = UIColor.clear
            softwareDeveloper=softwareDeveloper-3
        }
        else {
            answerFiveButton.backgroundColor = UIColor.black
            softwareDeveloper=softwareDeveloper+3
        }
    }
    
    @IBAction func answerSixButton(_ sender: Any) {
        if answerSixButton.backgroundColor == UIColor.black {
            answerSixButton.backgroundColor = UIColor.clear
            technicalSales=technicalSales-3
        }
        else {
            answerSixButton.backgroundColor = UIColor.black
            technicalSales=technicalSales+3
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "segueOne"{
            let myVC = segue.destination as? QuestionnairePart3ViewController
            myVC?.projectManager = projectManager
            myVC?.productionSupport=productionSupport
            myVC?.businessAnalyst=businessAnalyst
            myVC?.softwareDeveloper=softwareDeveloper
            myVC?.softwareTester=softwareTester
            myVC?.technicalSales=technicalSales
            navigationController?.pushViewController(myVC!, animated: true)
        }
    }
    
    
    
}


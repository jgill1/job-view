//
//  GlassdoorViewController.swift
//  Job View
//
//  Created by Joshua Gill on 26/03/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

class GlassdoorViewController: UIViewController{
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var logo_image: UIImageView!
    @IBOutlet weak var pros_text: UITextView!
    @IBOutlet weak var headline_text: UILabel!
    @IBOutlet weak var cons_text: UITextView!
    @IBOutlet var popView: UIView!
    @IBOutlet weak var background_image: UIImageView!
    @IBOutlet weak var rating_lbl: UILabel!
    
    var logoURL = ""
    var glassdoorHeadline = ""
    var glassdoorPro = ""
    var glassdoorCon = ""
    var glassdoorURL = ""
    var glassdoorRating = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Download Logo and set to logo imageview and background
        logo_image.sd_setImage(with: URL(string: logoURL), placeholderImage: nil, options: [.continueInBackground, .progressiveDownload])
        logo_image.layer.cornerRadius = 8.0
        logo_image.clipsToBounds = true
        
        
        pros_text.text = glassdoorPro
        cons_text.text = glassdoorCon
        headline_text.text = glassdoorHeadline
        
        background_image.layer.cornerRadius = 8.0
        background_image.clipsToBounds = true
        background_image.layer.borderWidth = 1.0
        background_image.layer.borderColor = UIColor.white.cgColor
        self.background_image.sd_setImage(with: URL(string: logoURL), placeholderImage: nil, options: [.continueInBackground, .progressiveDownload])
        self.background_image.addBlurEffect()
        
        popView.layer.cornerRadius = 8
        popView.layer.masksToBounds = true
        
        rating_lbl.text = "Rating: " + glassdoorRating
        
        pros_text.textContainerInset = UIEdgeInsetsMake(10,10,10,10)
        pros_text.layer.cornerRadius = 8
        pros_text.layer.borderWidth = 2
        pros_text.layer.borderColor = UIColor.black.cgColor
        
        cons_text.textContainerInset = UIEdgeInsetsMake(10,10,10,10)
        cons_text.layer.cornerRadius = 8
        cons_text.layer.borderWidth = 2
        cons_text.layer.borderColor = UIColor.black.cgColor
    }
    
    @IBAction func glassdoorWebsite(_ sender: Any) {
        if let url = URL(string: glassdoorURL) {
            let vc = SFSafariViewController(url: url)
            present(vc, animated: true)
        }
    }
    
    @IBAction func DismissButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

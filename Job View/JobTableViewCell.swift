//
//  JobTableViewCell.swift
//  Job View
//
//  Created by Joshua Gill on 14/02/2017.
//  Copyright © 2017 Joshua Gill. All rights reserved.
//

import UIKit

class JobTableViewCell: UITableViewCell {

    @IBOutlet weak var title_lbl: UILabel!
    @IBOutlet weak var location_lbl: UILabel!
    @IBOutlet weak var applications_lbl: UILabel!
    @IBOutlet weak var salary_lbl: UILabel!
    @IBOutlet weak var logo_image: UIImageView!
    @IBOutlet weak var daysLeft_lbl: UILabel!
    @IBOutlet weak var background_lbl: UILabel!
    
    @IBOutlet weak var sideColour_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

}
